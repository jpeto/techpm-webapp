//Run as: node server.js > output.log &
//info se ven en el output.log
//error y warn requieren el console

var express  = require('express'),
	util = require('util'),
	shell = require('shelljs'),
	fs = require('fs'),
	formidable = require('formidable'),
	path = require('path');
var app = express();

//ruta base ejemplo: E:\TechPm, de momento utilizar ruta local
var rutaBase = require('os').homedir(); //se traduce a 'C:\Users\Javier' por mencionar un ejemplo
//var rutaBase = "\\\\172.16.0.96\\Proyectos\\techpm";
var documentacion = "documentacion";
var evidencias = "evidencias";

app.get('/deleteFileSeccion/:idProyecto/:seccion/:deleteFile', function (req, res) {
	var filePath = rutaBase + path.sep + req.params.idProyecto + path.sep + req.params.seccion + path.sep + req.params.deleteFile; 
	fs.unlink(filePath,function(err){
        if (err) {
            console.log("failed to delete local image:"+err);
        } else {
            console.log('successfully deleted local image');                                
        }
	});
});

app.get('/fileExists/:idProyecto/:seccion/:nombreArchivo', function(req, res){
	var filePath = rutaBase + path.sep + req.params.idProyecto + path.sep + req.params.seccion + path.sep + req.params.nombreArchivo;
	res.writeHead('200', {'content-type':'text/plain'});
	res.end(fs.existsSync(filePath).toString());
});

app.get('/deleteFile', function(req, res){
	fs.unlink(req.query.ruta,function(err){
        if (err) {
            console.log("failed to delete local image:"+err);
        } else {
            console.log('successfully deleted local image');                                
        }
	});
});

app.get('/download', function(req, res){
	res.download(req.query.ruta);
});

app.get('/getRutaSeccion/:idProyecto/:seccion', function(req, res){
	res.writeHead('200', {'content-type':'text/plain'});
	res.end(rutaBase + path.sep + req.params.idProyecto + path.sep + req.params.seccion + path.sep);
});

app.get('/crearCarpetas/:idProyecto', function(req, res) {
	var rutaConId = rutaBase + path.sep + req.params.idProyecto;
	var documentacionRuta = rutaConId + path.sep + documentacion;
	var evidenciasRuta = rutaConId + path.sep + evidencias;
	shell.mkdir('-p', rutaConId, documentacionRuta, evidenciasRuta);
	res.end();
});

app.post('/uploadFiles/:idProyecto/:seccion', function(req, res, next){
	var form = new formidable.IncomingForm(),
		fields = [],files=[];
		form.uploadDir = rutaBase + path.sep + req.params.idProyecto + path.sep + req.params.seccion;
	form
		.on('error', function(err){
			res.writeHead(200, {'content-type':'text/plain'});
			res.end('error:\n\n'+util.inspect(err));
		})
		.on('field', function(field, value){
			//console.log(field, value);
			fields.push([field, value]);
		})
		.on('file', function(field, file) {
			console.log(file);
			fs.rename(file.path, form.uploadDir + path.sep + file.name);
			files.push([field, file]);
		})
		.on('end', function(){
			console.log('-> post done');
			res.writeHead('200', {'content-type':'text/plain'});
			res.end('200');
		});
	//console.log(req);
	form.parse(req);
});

app.post('/uploadImageClient/:seccion', function(req, res, next){
	var form = new formidable.IncomingForm(),
		fields = [],files=[];
		form.uploadDir = rutaBase + path.sep + req.params.seccion;
		//form.uploadDir = "C:\\Users\\Jose Antonio\\123";
	form
		.on('error', function(err){
			res.writeHead(200, {'content-type':'text/plain'});
			res.end('error:\n\n'+util.inspect(err));
		})
		.on('field', function(field, value){
			//console.log(field, value);
			fields.push([field, value]);
		})
		.on('file', function(field, file) {
			console.log(file);
			fs.rename(file.path, form.uploadDir + path.sep + file.name);
			files.push([field, file]);
		})
		.on('end', function(){
			console.log('-> post done');
			res.writeHead('200', {'content-type':'text/plain'});
			res.end('received fields:\n\n'+util.inspect(fields) +'\n\n received files: ' + util.inspect(files));
		});
	//console.log(req);
	form.parse(req);
});

app.get('/getRutaSeccionCliente/:seccion', function(req, res){
	res.writeHead('200', {'content-type':'text/plain'});
	res.end(rutaBase + path.sep + req.params.seccion + path.sep);
});

app.get('/getImagenCliente',function(req,res){ 
    res.sendFile(req.query.rutaImagenCliente);
});

var JL = require('jsnlog').JL;         
var jsnlog_nodejs = require('jsnlog-nodejs').jsnlog_nodejs;
var bodyParser = require('body-parser'); 

app.use(bodyParser.json());
app.post('*.logger', function (req, res) { 
	jsnlog_nodejs(JL, req.body);
		console.log(req.body.lg[0].n+': '+req.body.lg[0].m);
		res.send('');
});

app.use(express.static(__dirname + '/'));
//exponer componentes (views, controllers, scripts js etc) ubicados en ../scripts
//app.use('/scripts',  express.static(__dirname + '/scripts'));           
app.listen(8084);
console.log("TechPM corre en el puerto 8084");

