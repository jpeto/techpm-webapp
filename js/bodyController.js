app.controller('bodyController', ['$scope', '$location', '$rootScope', 'AUTH_EVENTS', 'autenticacion', '$window', '$cookieStore',
                                  function($scope, $location, $rootScope, AUTH_EVENTS, autenticacion, $window, $cookieStore ){

    //Usuario inicial
	$scope.currentUser = null;
                                      
    //Funcion para establecer usuario actual al hacer login
	var setCurrentUser = function(){
		$scope.currentUser = $rootScope.currentUser;
	}
    var logoutListener = function(){
        $scope.currentUser = $rootScope.currentUser;
		$location.path('/TechPM');
	}

	//Escuchando por evento de login o logout
    $rootScope.$on(AUTH_EVENTS.nolog, $location.path('/TechPM'));
	$rootScope.$on(AUTH_EVENTS.logoutSuccess, logoutListener);
	$rootScope.$on(AUTH_EVENTS.loginSuccess, setCurrentUser);   
                                      
    //Si hay una sesion iniciada, al hacer refresh inicia de nuevo 
    if ($window.sessionStorage["userInfo"]) {
		var credentials = JSON.parse($window.sessionStorage["userInfo"]);
		autenticacion.refresh(credentials, $cookieStore.get('vista'), function(user) {
            $rootScope.enlogin = false;
            $location.path($cookieStore.get('vista'));
		});
	}

} ]);