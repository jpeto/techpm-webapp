app.controller('menuEsquina', ['$scope', '$location', '$rootScope', '$cookieStore', '$uibModal',
	function ($scope, $location, $rootScope, $cookieStore, $uibModal) { 

	//Funcion para salir de la sesion
	//$rootScope.currentUser.rol = 1;
	$scope.secciones = [];
	if ($cookieStore.get('vista') == '/TechPM/Clientes' && $rootScope.currentUser.rol == 1){    	
		$scope.secciones.push({ruta:"#/TechPM", decripcion:"Agregar Cliente", icono:"glyphicon glyphicon-folder-open", 
			icono2:"glyphicon glyphicon-text-background"});
		$scope.secciones.push({ruta:"#/TechPM", decripcion:"Agregar Herramienta", icono:"glyphicon glyphicon-wrench", 
			icono2:"glyphicon glyphicon-text-background"});
	}else if ($cookieStore.get('vista') == '/TechPM/Clientes' && $rootScope.currentUser.rol != 1) {
		$scope.secciones.push({ruta:"#/TechPM", decripcion:"Agregar Herramienta", icono:"glyphicon glyphicon-wrench", 
			icono2:"glyphicon glyphicon-text-background"});
	}else if ($cookieStore.get('vista') == '/TechPM/Proyectos' && $rootScope.currentUser.rol == 1){
		$scope.secciones.push({ruta:"/TechPM/Clientes", decripcion:"Clientes", icono:"glyphicon glyphicon-chevron-up"});
		$scope.secciones.push({ruta:"#/TechPM", decripcion:"Agregar Proyecto", icono:"glyphicon glyphicon-briefcase", 
			icono2:"glyphicon glyphicon-text-background"});
		$scope.secciones.push({ruta:"/TechPM/AdministracionRecursos", decripcion:"Administrar Recursos", icono:"glyphicon glyphicon-user", 
			icono2:"glyphicon glyphicon-cog"});
		$scope.secciones.push({ruta:"/TechPM/ProyectosCerrados", decripcion:"Proyectos Cerrados", icono:"glyphicon glyphicon-briefcase", 
			icono2:"glyphicon glyphicon-ban-circle"});
		$scope.secciones.push({ruta:"/TechPM/CredencialesCliente", decripcion:"Credenciales", icono:"glyphicon glyphicon-credit-card"});

	}else if ($cookieStore.get('vista') == '/TechPM/Proyectos' && $rootScope.currentUser.rol != 1){
		$scope.secciones.push({ruta:"/TechPM/Clientes", decripcion:"Clientes", icono:"glyphicon glyphicon-chevron-up"});
		$scope.secciones.push({ruta:"/TechPM/ProyectosCerrados", decripcion:"Proyectos Cerrados", icono:"glyphicon glyphicon-briefcase", 
			icono2:"glyphicon glyphicon-ban-circle"});
		//$scope.secciones.push({ruta:"/TechPM/CredencialesCliente", decripcion:"Credenciales", icono:"glyphicon glyphicon-credit-card"});
	}else if ($cookieStore.get('vista') == '/TechPM/ProyectosCerrados'){
		$scope.secciones.push({ruta:"/TechPM/Proyectos", decripcion:$rootScope.clienteSeleccionado.nombre, 
			icono:"glyphicon glyphicon-chevron-up"});    	
		$scope.secciones.push({ruta:"/TechPM/Clientes", decripcion:"Clientes", icono:"glyphicon glyphicon-folder-open"});

	}else if ($cookieStore.get('vista') == '/TechPM/AdministracionRecursos'){
		$scope.secciones.push({ruta:"/TechPM/Proyectos", decripcion:$rootScope.clienteSeleccionado.nombre, 
			icono:"glyphicon glyphicon-chevron-up"});
		$scope.secciones.push({ruta:"/TechPM/Clientes", decripcion:"Clientes", icono:"glyphicon glyphicon-folder-open"});

	}else if ($cookieStore.get('vista') == '/TechPM/CredencialesCliente'){
		$scope.secciones.push({ruta:"/TechPM/Proyectos", decripcion:$rootScope.clienteSeleccionado.nombre, 
			icono:"glyphicon glyphicon-chevron-up"});
		$scope.secciones.push({ruta:"/TechPM/Clientes", decripcion:"Clientes", icono:"glyphicon glyphicon-folder-open"});

	}else if ($cookieStore.get('vista') == '/TechPM/DetalleProyecto'){
		$scope.secciones.push({ruta:"/TechPM/DocumentacionProyecto", decripcion:"Documentación", icono:"glyphicon glyphicon-book"});
		$scope.secciones.push({ruta:"/TechPM/BitacoraProyecto", decripcion:"Bitácora", icono:"glyphicon glyphicon-time"});
		$scope.secciones.push({ruta:"/TechPM/BranchesProyecto", decripcion:"Branches", icono:"glyphicon glyphicon-list"});
		$scope.secciones.push({ruta:"/TechPM/Proyectos", decripcion:$rootScope.clienteSeleccionado.nombre, 
			icono:"glyphicon glyphicon-chevron-up"});
		$scope.secciones.push({ruta:"/TechPM/Clientes", decripcion:"Clientes", icono:"glyphicon glyphicon-folder-open"});

	}else if ($cookieStore.get('vista') == '/TechPM/BitacoraProyecto'){
		$scope.secciones.push({ruta:"/TechPM/DetalleProyecto", decripcion:"Detalle", icono:"glyphicon glyphicon-file"});
		$scope.secciones.push({ruta:"/TechPM/DocumentacionProyecto", decripcion:"Documentación", icono:"glyphicon glyphicon-book"});
		$scope.secciones.push({ruta:"/TechPM/BranchesProyecto", decripcion:"Branches", icono:"glyphicon glyphicon-list"});
		$scope.secciones.push({ruta:"/TechPM/Proyectos", decripcion:$rootScope.clienteSeleccionado.nombre, 
			icono:"glyphicon glyphicon-chevron-up"});
		$scope.secciones.push({ruta:"/TechPM/Clientes", decripcion:"Clientes", icono:"glyphicon glyphicon-folder-open"});

	}else if ($cookieStore.get('vista') == '/TechPM/BranchesProyecto'){
		$scope.secciones.push({ruta:"/TechPM/DetalleProyecto", decripcion:"Detalle", icono:"glyphicon glyphicon-file"});
		$scope.secciones.push({ruta:"/TechPM/DocumentacionProyecto", decripcion:"Documentación", icono:"glyphicon glyphicon-book"});
		$scope.secciones.push({ruta:"/TechPM/BitacoraProyecto", decripcion:"Bitácora", icono:"glyphicon glyphicon-time"});
		$scope.secciones.push({ruta:"/TechPM/Proyectos", decripcion:$rootScope.clienteSeleccionado.nombre, 
			icono:"glyphicon glyphicon-chevron-up"});
		$scope.secciones.push({ruta:"/TechPM/Clientes", decripcion:"Clientes", icono:"glyphicon glyphicon-folder-open"});

	}else if ($cookieStore.get('vista') == '/TechPM/DocumentacionProyecto'){
		$scope.secciones.push({ruta:"/TechPM/DetalleProyecto", decripcion:"Detalle", icono:"glyphicon glyphicon-file"});
		$scope.secciones.push({ruta:"/TechPM/BitacoraProyecto", decripcion:"Bitácora", icono:"glyphicon glyphicon-time"});
		$scope.secciones.push({ruta:"/TechPM/BranchesProyecto", decripcion:"Branches", icono:"glyphicon glyphicon-list"});
		$scope.secciones.push({ruta:"/TechPM/Proyectos", decripcion:$rootScope.clienteSeleccionado.nombre, 
			icono:"glyphicon glyphicon-chevron-up"});
		$scope.secciones.push({ruta:"/TechPM/Clientes", decripcion:"Clientes", icono:"glyphicon glyphicon-folder-open"});

	}else if ($cookieStore.get('vista') == '/TechPM/'){
		$scope.secciones.push({ruta:"#/TechPM", decripcion:"", icono:""});
	}

	$scope.mostrarMenu = function(){
		return $rootScope.currentUser.rol == 1 && $cookieStore.get('vista') == '/TechPM/Clientes';
	}

	$scope.mostrarHover = false;
	$scope.descripcion = "";
	$scope.hover = function(descripcion){
		$scope.mostrarHover = true;
		$scope.descripcion = descripcion;
	}
	$scope.noHover = function(descripcion){
		$scope.mostrarHover = false;
		$scope.descripcion = "";
	}

	var modalInstance; 
	var instanciaModal;
	$scope.agregarCliente = function(test) {
		
		modalInstance = null;
		instanciaModal = null;
		if (test.ruta != "#/TechPM"){
			$location.path(test.ruta);
		} else {
			if (test.decripcion == 'Agregar Cliente'){
				instanciaModal = 1;
				modalInstance = $uibModal.open({
					templateUrl: 'views/agregarClientes.html',
					controller: 'agregarClientes',
					backdrop: 'static',
					size: 'lg',
					animation: true
				});
			}else if (test.decripcion == 'Agregar Herramienta'){
				instanciaModal = 2;
				modalInstance = $uibModal.open({
					templateUrl: 'views/agregarHerramienta.html',
					controller: 'agregarHerramienta',
					backdrop: 'static',
					size: 'lg',
					animation: true
				});
				//console.log(test);
			} else if (test.decripcion == 'Agregar Proyecto'){
				instanciaModal = 3;
				modalInstance = $uibModal.open({
					templateUrl: 'views/agregarProyecto.html',
					controller: 'agregarProyecto',
					backdrop: 'static',
					size: 'lg',
					animation: true
				});
			}
		}		
	}

	$scope.$on('cerrar-modal', function (event, args) {
		if (instanciaModal == 1){
			$rootScope.$broadcast('actualizar-clientes', args);			
		} else if (instanciaModal == 3) {
			$rootScope.$broadcast('actualizar-proyectos', args);	
		}
		modalInstance.result.then(function () {
		}, function () {
			//console.log('cerrar-modal');
		});
	});
}])