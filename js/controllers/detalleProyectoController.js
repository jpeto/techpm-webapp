app.controller('detalleProyecto', [ '$scope', '$location', '$rootScope', '$cookieStore', '$http', '$window', 'Flash', '$timeout',
	function($scope, $location, $rootScope, $cookieStore, $http, $window, Flash, $timeout) {
    loginData = $rootScope.currentUser;
	$cookieStore.put('vista', $location.path()); 
		
	$scope.herramientas = [];
	$scope.detallesProyecto = [];
	$scope.detallesProyecto.descripcion = "";
	$scope.detallesProyecto.detalleEstatus = "";
	$scope.detallesProyecto.estatus = "";
	
	$scope.imagen = "";
	$scope.detalleEstatus = "";
	
	//Mensajes De Respuesta
	$scope.DatosProyectoActualizadoError = "";
	
    $scope.success = function () {
        var message = '<strong>Excelente!</strong> El cambio para el Proyecto <strong>'+ $scope.proyectoActualizado.data.nombreProyecto + '</strong> fue realizado con exito, Estatus: ' 
        	+ '<strong>' + $scope.proyectoActualizado.status + '-' + $scope.proyectoActualizado.statusText + '</strong>';
        Flash.create('success', message);
    };
    
    $scope.danger = function () {
        var message = '<strong>Lo sentimos!</strong> El proceso no se pudo completar, error : ' 
        		+ '<strong>' + $scope.DatosProyectoActualizadoError.status + '-' + $scope.DatosProyectoActualizadoError.message + '</strong>';
        Flash.create('danger', message);
    };
	
    //Propiedades para Drag and Drop
    $scope.models = {
            selected: null,
            lists: {"Elegidas": [], "Disponibles": []}
        };
    
    //Carga de informacion proyecto junto con las herramientas disponibles y utilizadas
	$scope.initDetalleProyecto = function () {
		$http.get($rootScope.url + 'buscarProyectoXId/'+ $rootScope.proyectoSeleccionado,
				{headers: {'Authorization': 'Bearer ' + loginData.access_token}})                 
			.then(function successCallback(responseProyecto) {
				$scope.detallesProyecto = responseProyecto.data;
				//console.log($scope.models.lists.Elegidas);
				$http.get($rootScope.url + 'buscarHerramienta', 
                          {headers: {'Authorization': 'Bearer ' + loginData.access_token}})                 
					.then(function successCallback(response) {
						$scope.herramientas = response.data;
					    var buscaHerramienta;
					    var index;
					    var herramientasDisponibles = [];
					    //BUSCA TODAS LAS HERRAMIENTAS QUE ESTAN DISPONIBLES PARA ASIGNAR AL PROYECTO
					    for (var i = 0; i < $scope.herramientas.length; ++i) {
					    	$scope.models.lists.Disponibles.push($scope.herramientas[i].herramienta);
					    	herramientasDisponibles.push($scope.herramientas[i].herramienta);
					    }
					    // Funcion para validar si existen Herramientas de el proyecto seleccionado
					    if ($scope.detallesProyecto.herramientas.length > 0) {
					    	//BUSCA TODAS LAS HERRAMIENTAS QUE ESTAN ELEGIDAS PARA EL PROYECTO
						    for (var i = 0; i < $scope.detallesProyecto.herramientas.length; i++) {
						    	 $scope.models.lists.Elegidas.push($scope.detallesProyecto.herramientas[i]);
							}
						    //Busca todas las Herramientas elejidas y las elimina de las herramientas disponibles para que no se vuelvan a mostrar
						    for (var i = 0; i < $scope.models.lists.Elegidas.length; i++) {
						    	buscaHerramienta = $scope.models.lists.Elegidas[i];
						    	index = herramientasDisponibles.indexOf(buscaHerramienta);
						    		if (index !== -1) {
						    			$scope.models.lists.Disponibles.splice(index,1);
						    			herramientasDisponibles.splice(index,1);
						    		}	
						    }
						}else {
							//console.log("NO HAY HERRAMIENTAS ELEGIDAS");
						}
					    $scope.showSuccessAlert = false;
					    $scope.EstadoImagen();
					}, function errorCallback(response) {
						JL("ERROR").error("Error al consultar servicio: buscarHerramienta, response: "+  response.data.error);
					});
			}, function errorCallback(response) {
				JL("ERROR").error("Error al consultar servicio: buscarProyectoXId, response: "+  response.data.error);
			});
	}
	
	$scope.initDetalleProyecto();
    
	//Cambio de imagen al seleccionar un estado para el proyecto
    $scope.EstadoImagen = function() {
    	if ($scope.detallesProyecto.estatus == null) {
    		$scope.imagen = "Images/vacio.png";
		}
    	else if ($scope.detallesProyecto.estatus == "Iniciado") 
		{
			$scope.imagen = "Images/Iniciado.png";
		}
		else if ($scope.detallesProyecto.estatus == "En Proceso")
		{
			$scope.imagen = "Images/EnProceso.png";
		}
		else if ($scope.detallesProyecto.estatus == "Terminado") 
		{
			$scope.imagen = "Images/Terminado.png";
		}
	}
	
    // Model to JSON for demo purpose
    $scope.$watch('models.lists', function(model) {
        $scope.modelAsJson = angular.toJson(model, true);
    }, true);

    $scope.onChanged = function() {
		if ($scope.detallesProyecto.estatus == "Iniciado") 
		{
			$scope.imagen = "Images/Iniciado.png";
		}
		else if ($scope.detallesProyecto.estatus == "En Proceso")
		{
			$scope.imagen = "Images/EnProceso.png";
		}
		else if ($scope.detallesProyecto.estatus == "Terminado") 
		{
			$scope.imagen = "Images/Terminado.png";
		}
    }
    
    //Funcion para actualizar el proyecyto
	$scope.actualizarProyecto = function() {
		delete $scope.detallesProyecto.herramientas;
		$scope.detallesProyecto.herramientas = $scope.models.lists.Elegidas;
		$http.put($rootScope.url + 'actualizarProyecto', $scope.detallesProyecto,
				{headers: {'Authorization': 'Bearer ' + loginData.access_token}})
					.then(function successCallBack(responseProyecto) {
							$scope.proyectoActualizado = responseProyecto;
							$scope.success();
							$scope.showSuccessAlert = true;
							$scope.successTextAlert = "¡La informacion se almaceno de manera correcta!";
						},
						function errorCallBack(responseProyecto) {
							$scope.DatosProyectoActualizadoError = responseProyecto.data;
							$scope.danger();
							JL("ERROR").error("Error al consultar servicio: actualizarProyecto, response: "+  response.data.error);
						});
		}
    
}]);