app.controller('login', [ '$scope', '$location', '$rootScope', 'autenticacion', 
	function($scope, $location, $rootScope, autenticacion) {
    
    //Para ocultar header en vista de login
    $rootScope.enlogin = true;
    $rootScope.url = 'http://172.16.0.68:9021/techpm-service/';
    $rootScope.securityurl = 'http://172.16.0.68:9021/security/';
    
    //Variables para ng-models del login
	$scope.credentials = {};
	$scope.loginForm = {};
	$scope.error = false;
    $scope.errorMsj = ""
    	
	//Enviar formulario del login con datos del usuario
	$scope.submit = function() {
		$scope.submitted = true;
		if (!$scope.loginForm.$invalid) {
			$scope.login($scope.credentials);              
		} else {
            return;
        }
	};

	//Funcion para iniciar sesion
	$scope.login = function(credentials) {  
		$scope.error = false;
		autenticacion.login(credentials, function(user) {
            $rootScope.enlogin = false;
            $rootScope.$broadcast('actualizar-header', 'actualizar-header');
            $location.path('/TechPM/Clientes');
		}, function(err) {
            $scope.error = true;
            if (err == 1) {
                $scope.errorMsj = "Nombre de usuario y/o contraseña incorrecto(s).";
            } else {
                $scope.errorMsj = "Error al autenticar usuario, intente de nuevo.";
            }           
		});
	};
}]);
