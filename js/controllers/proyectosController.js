app.controller('proyectos', [ '$scope', '$location', '$rootScope', '$cookieStore', '$http', '$window', '$sce', '$compile',
	function($scope, $location, $rootScope, $cookieStore, $http, $window, $sce, $compile) {
    
	$cookieStore.put('vista', $location.path());
	loginData = $rootScope.currentUser;
	
	$scope.$on('actualizar-proyectos', function (event, args) {
		$scope.initMenuProyecto();
    });
	
	$rootScope.banderaImagen = 1;
	
	$scope.initMenuProyecto = function (){
		$http.get($rootScope.url + 'buscarProyecto/' + $rootScope.currentUser.login + '/' + $rootScope.clienteSeleccionado.nombreCliente,
				{headers: {'Authorization': 'Bearer ' + loginData.access_token}})
		.then(
				function successCallback(response) {
					$scope.proyectos = response.data;
					$scope.datoForm = "\getImagenCliente";
					var text = "";
					var dato;
					var iniciohtml = '<div class="container"> <script type="text/javascript" src="libs/menuCirculo/jquery-1.12.4.js"></script>' +
						'<script type="text/javascript" src="libs/menuCirculo/orbitlist.js"></script><ul class="orbit">';
					for(var i = 0; i < $scope.proyectos.length; i++){
					    if($scope.proyectos[i].visible){
					       text = text + '<li id="nom'+$scope.proyectos[i].nombreProyecto+'">'+'<input type="checkbox">' +
	                        	'<label>'+ $scope.proyectos[i].nombreProyecto+ '</label>'+
	                        	'<ul>'+
	                        		'<li id="DETALLES">'+
	                        			'<input type="checkbox">' +
	                        			'<label id="submenu" ng-click="irAMenuSeleccionado('+$scope.proyectos[i].idProyecto+',$event)" data="DETALLES">DETALLES</label>'+
	                        		'</li>'+
	                        		'<li id="BITACORA">'+
	                        				'<input type="checkbox">' +
	                        				'<label id="submenu" ng-click="irAMenuSeleccionado('+$scope.proyectos[i].idProyecto+',$event)" data="BITACORA">BITACORA</label>'+
	                        		'</li>'+
	                        		'<li id="BRANCH">'+
	                        			'<input type="checkbox">' +
	                        			'<label id="submenu" ng-click="irAMenuSeleccionado('+$scope.proyectos[i].idProyecto+',$event)" data="BRANCH">BRANCH</label>'+
	                        		'</li>'+
	                        		'<li id="DOC">'+
	                        			'<input type="checkbox">' +
	                        			'<label id="submenu" ng-click="irAMenuSeleccionado('+$scope.proyectos[i].idProyecto+',$event)" data="DOCUMENTOS">DOCUMENTOS</label>'+
	                        		'</li>'+
	                        	'</ul>'+
							'</li>';
					    }
				    }
						$scope.trustAsHtml = iniciohtml + text + '</ul></div>';
				},
				function errorCallback(response) {
					JL("ERROR").error("Error al consumir servicio /buscarProyecto");					
				}
		);
	}
	
	$scope.initMenuProyecto();
		
	$scope.irAMenuSeleccionado = function(test, subM){
		var dataValue = subM.currentTarget.attributes.data.nodeValue;
		$rootScope.proyectoSeleccionado = test;
		if (dataValue == "DETALLES") {
			$window.sessionStorage["proyectoSeleccionado"] = JSON.stringify($rootScope.proyectoSeleccionado);
			$location.path("/TechPM/DetalleProyecto");
		}else if (dataValue == "BITACORA") {
			$window.sessionStorage["proyectoSeleccionado"] = JSON.stringify($rootScope.proyectoSeleccionado);
			$location.path("/TechPM/BitacoraProyecto");
		}else if (dataValue == "BRANCH") {
			$window.sessionStorage["proyectoSeleccionado"] = JSON.stringify($rootScope.proyectoSeleccionado);
			$location.path("/TechPM/BranchesProyecto");
		}else if (dataValue == "DOCUMENTOS") {
			$window.sessionStorage["proyectoSeleccionado"] = JSON.stringify($rootScope.proyectoSeleccionado);
			$location.path("/TechPM/DocumentacionProyecto");
		}
	}
}]);
