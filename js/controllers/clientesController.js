app.controller('clientes', [ '$scope', '$location', '$rootScope', '$cookieStore', '$http' , '$window',
	function($scope, $location, $rootScope, $cookieStore, $http, $window) {
    
	$cookieStore.put('vista', $location.path()); 
	loginData = $rootScope.currentUser;
	$scope.$on('actualizar-clientes', function (event, args) {
		$scope.initMenuClientes();
    });
	
	$rootScope.myproject = [];
	
	$rootScope.banderaImagen = 0;
	
	$scope.initMenuClientes = function() {
		$http
		.get(
				$rootScope.url + 'buscarClientesXUsuario/' + $rootScope.currentUser.login,
				{headers: {'Authorization': 'Bearer ' + loginData.access_token}})
		.then(
				function successCallback(response) {
					$scope.cliente = response.data;
					var text = "";
					var dato;
					var iniciohtml = '<div class="container"> <script type="text/javascript" src="libs/menuCirculo/jquery-1.12.4.js"></script>' +
						'<script type="text/javascript" src="libs/menuCirculo/orbitlist.js"></script><ul class="orbit">';
					for(var i = 0; i < $scope.cliente.length; i++){
					text = text + '<li id="nom'+$scope.cliente[i].nombre+'">'+'<input type="checkbox">' +
	                        	'<label ng-click="irAProyecto('+$scope.cliente[i].idCliente+',$event)" data="'+$scope.cliente[i].nombre+'" value="'+$scope.cliente[i].imagen+'">'+ $scope.cliente[i].nombre+ '</label>'+
	                        '</li>';
					}
					$scope.trustAsHtml = iniciohtml + text + '</ul></div>';
				},
				function errorCallback(response) {
					JL("ERROR")
							.error(
									"Error al consumir servicio /buscarClientesXUsuario , response: "+  response.data.error);
				});	
	}
	
	$scope.initMenuClientes();
	
	$scope.irAProyecto = function(idcliente, test, imagen) {
		var dataValue = test.currentTarget.attributes.data.nodeValue;
		var dataValueUrl = test.currentTarget.attributes.value.nodeValue;
		$rootScope.clienteSeleccionado = {nombreCliente : dataValue, idCliente : idcliente, imagen : dataValueUrl};
		$window.sessionStorage["clienteSeleccionado"] = JSON.stringify($rootScope.clienteSeleccionado);
		$location.path("/TechPM/Proyectos");
	}	
}]);
