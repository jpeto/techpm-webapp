app.controller('proyectosCerrados', ['$scope', '$location', '$rootScope', '$cookieStore', 'Flash', '$http',
	function($scope, $location, $rootScope, $cookieStore,Flash,$http) {
    
	$cookieStore.put('vista', $location.path()); 
        loginData = $rootScope.currentUser;
	$scope.disabled = $rootScope.currentUser.rol != 1;
	$scope.successTextAlert = '';
	$scope.proyectosCerrados = [];

    $scope.initProyectoCerrado = function (){
		
		$http.get($rootScope.url + 'buscaProyectosCerrados/' + $rootScope.clienteSeleccionado.nombreCliente, 
                  {headers: {'Authorization': 'Bearer ' + loginData.access_token}})                 
        .then(function successCallback(response) {
           if (response.data != ""){
        	   $scope.proyectosCerrados = response.data;
           }
         }, function errorCallback(response) {
           JL("ERROR").error("Error al consultar servicio: buscaProyectosCerrados, response: "+response);
       });
	}

    $scope.initProyectoCerrado();

	console.log($rootScope.clienteSeleccionado);

	$scope.enabled = true;
	$scope.irAProyecto = function(proyecto){
		//$window.sessionStorage["proyectoSeleccionado"] = JSON.stringify($rootScope.proyectoSeleccionado);
		$rootScope.proyectoSeleccionado = proyecto.idProyecto;
		$location.path("/TechPM/DetalleProyecto");
        console.log($rootScope.proyectoSeleccionado)
	};
	
	$scope.cambiaVisibilidad = function(proyecto,visibilidad){
		console.log(proyecto);
		proyecto.visible = visibilidad;
		JL("INFO").info("Proyecto Visible: " + visibilidad);
		$http.put($rootScope.url + 'actualizarProyecto', proyecto,
            {headers: {'Authorization': 'Bearer ' + loginData.access_token}})                 
        .then(function successCallback(response) {
           if (response.data != ""){
			$scope.success(visibilidad,proyecto.nombreProyecto);
			$scope.showSuccessAlert = true;
			$scope.initProyectoCerrado();
			JL("INFO").info("Proyecto Actualizado: ");
           }
         }, function errorCallback(response) {
           JL("ERROR").error("Error al consumir servicio: actualizarProyecto, response: "+response);
       });
		
	}

	$scope.success = function (visibilidad,nombre) {
		var visible = ' No sera Visible';
		if(visibilidad){
			visible = ' estara Visible';
		}
        var message = 'El proyecto - '+ nombre + '' + visible;
        Flash.create('success', message);
    };

}]);