app.controller('agregarProyecto', [ '$scope', '$location', '$rootScope','$window', '$cookieStore', '$http','$uibModalInstance', 'Flash', '$timeout',
	function($scope, $location, $rootScope, $window, $cookieStore, $http, $uibModalInstance,Flash,$timeout) {
    loginData = $rootScope.currentUser;
	$cookieStore.put('vista', $location.path());

	$scope.proyecto = {};
	$scope.proyectoNombre= "";
	
	//Limpiar Variables 
	var limpiarPopProyecto = function() {
		$scope.proyectoNombre= "";
	}
	
	//Mensajes De Respuesta
	$scope.DatosProyectoGuardadoError = "";
	
    $scope.success = function () {
        var message = '<strong>Excelente!</strong> El Proyecto : <strong>'+ $scope.proyectoGuardado.data.nombreProyecto + '</strong> fue agregado con exito, Estatus: ' 
        		+ '<strong>' + $scope.proyectoGuardado.status + '-' + $scope.proyectoGuardado.statusText + '</strong>';
        Flash.create('success', message);
    };
    
    $scope.danger = function () {
        var message = '<strong>Lo sentimos!</strong> El proceso no se pudo completar, error : ' 
        		+ '<strong>' + $scope.DatosProyectoGuardadoError.status + '-' + $scope.DatosProyectoGuardadoError.message + '</strong>';
        Flash.create('danger', message);
    };
    
    $scope.warning = function () {
        var message = '<strong>Lo sentimos!</strong> El proyecto : ' 
        		+ '<strong>' + $scope.proyectoNombre + '</strong> Ya existe';
        Flash.create('warning', message);
    };

	$scope.init = function() {
		   
		$http.get($rootScope.url + 'buscarProyecto/' + $rootScope.currentUser.login + '/' + $rootScope.clienteSeleccionado.nombreCliente,
				{headers: {'Authorization': 'Bearer ' + loginData.access_token}})
				.then(function successCallback(response) {
							if (response.data != ""){
								$scope.proyectos = response.data;
							}
						},
						function errorCallback(response) {
							JL("ERROR")
							.error(
									"Error al consultar servicio: buscarProyecto/ , response: "+  response.data.error);
						});
	}


	$scope.init();

	$scope.guardar = function() {
		var n=0;
			var ingresada=$scope.proyectoNombre;
			for (var i = 0; i < $scope.proyectos.length; i++) {
				if($scope.proyectos[i].nombreProyecto!=null){

					var proyecto=$scope.proyectos[i].nombreProyecto;
					var compara = angular.equals(
							proyecto.toUpperCase(),
							ingresada.toUpperCase());
					if(compara==true){
						n++;
					}
				}
			}
			//Validar proyecto exista
			if(n==0){	
				var proyecto = {}; 
				proyecto.cliente = $rootScope.clienteSeleccionado.nombreCliente;
				proyecto.nombreProyecto = ingresada;
				proyecto.administradores = [{usuario: $rootScope.currentUser.login}];
				proyecto.herramientas = [];
				proyecto.branches = [];
				proyecto.documentos = [];
				proyecto.recursos = [];
				proyecto.visible = true;
				proyecto.cambios=[{evidencias: []}];
				
				$http.put($rootScope.url+ 'guardarProyecto',proyecto,
						{headers: {'Authorization': 'Bearer ' + loginData.access_token}}
					).then(function successCallBack(responseProy) {
							$scope.proyecto = responseProy.data;
							$scope.proyectoGuardado = responseProy;
							$scope.idProyecto = $scope.proyecto.idProyecto;
							$scope.crearCarpetas();
							$scope.init();
							$scope.success();
							limpiarPopProyecto();
						},
						function errorCallBack(responseProy) {
							$scope.DatosProyectoGuardadoError = responseProy.data;
							$scope.danger();
							limpiarPopProyecto();
							JL("ERROR").error("Error al consultar servicio: guardarProyecto/ , response: "+  responseProy.data.error);
						});
			}else {
				$scope.warning();
				limpiarPopProyecto();
				n=0;
			}
	}


	$scope.cerrar = function(){
		$uibModalInstance.dismiss('cancel');
		$rootScope.$broadcast('cerrar-modal', {cancel:'cancel'});
	}

	$scope.crearCarpetas = function()
	{
	    var xmlHttp = new XMLHttpRequest();
	    xmlHttp.open( "GET", "/crearCarpetas/"+$scope.idProyecto, true );
	    xmlHttp.send( null );
	    return xmlHttp.responseText;
	}
	
}]);