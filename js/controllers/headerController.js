app.controller('header', ['$scope','$rootScope', 'autenticacion', '$http','Session', '$cookieStore', '$location', 'Flash', '$timeout',
	function ($scope, $rootScope, autenticacion, $http, Session, $cookieStore, $location, Flash,$timeout) { 

	$cookieStore.put('vista', $location.path());
	$scope.ConocimientosUser=[];//conocimientos del usuario
	$scope.herramientas=[];//total de herramientas
	$scope.Listaconocimientos=[];//solo las herramientas que puede elegir el usuario
	$scope.Niveles=['Alto','Medio','Bajo'];
	$scope.nombre="";
	
	$scope.Noagregado=false;
	$scope.Noherramientas="";
	
	  $scope.success = function () {
	        var message = '<strong>Excelente!</strong> La información fue actualizada con exito, Estatus: ' 
	        		+ '<strong>' + $scope.proyectoGuardado.status + '-' + $scope.proyectoGuardado.statusText + '</strong>';
	        Flash.create('success', message,4000,{container: 'flash1'});
	    };
	    
	    $scope.danger = function () {
	        var message = '<strong>Lo sentimos!</strong> El proceso no se pudo completar, error : ' 
	        		+ '<strong>' + $scope.DatosProyectoGuardadoError.status + '-' + $scope.DatosProyectoGuardadoError.message + '</strong>';
	        Flash.create('danger', message,4000,{container: 'flash1'});
	    };
	    
	    $scope.warning = function () {
	        var message = '<strong>Lo sentimos!</strong> el usuario no tiene información';
	        Flash.create('warning', message,4000,{container: 'flash1'});
	    };
	
	
	//Funcion para salir de la sesion
	$scope.logout = function(){
		autenticacion.logout();
		$rootScope.enlogin = true;
		//console.log(autenticacion.isAuthenticated());
	};

	$scope.Init = function(){
        loginData = $rootScope.currentUser;
		$scope.ConocimientosUser=[];
		$scope.herramientas=[];
		$scope.Listaconocimientos=[];
		$scope.opcionesNuevo=false;
		$scope.Noherramientas="";
		
		$scope.opcionesAgregar=true;
		$http.get($rootScope.url+ 'buscarRecursoXUsuario/'+$rootScope.currentUser.login,
                  {headers: {'Authorization': 'Bearer ' + loginData.access_token}})
			.then(function successCallback(response) {
				if (response.data != ""){
					$scope.recurso = response.data;
					$scope.ConocimientosUser= $scope.recurso.conocimientos;
					$scope.nombre= $scope.recurso.nombre;
					//console.log( $scope.ConocimientosUser);
				}
				if(response.data.conocimientos.length==0){
					$scope.warning();
					
				}

				//OBTENER el catalógo de herramientas
				$http.get($rootScope.url+ 'buscarHerramienta',
                          {headers: {'Authorization': 'Bearer ' + loginData.access_token}})
					.then(function successCallback(response) {
						if (response.data != ""){
							$scope.herramientas = response.data;
							

						}
						else{
							//alert("No existen herramientas disponibles");
						}
						//Condicion para decir que el usuario no tiene conocimientos
						if($scope.ConocimientosUser!=null)
						{

							var n=0;
							var con= $scope.ConocimientosUser.length;
							//Comparar los conocimientos del usuario con el catálogo de herramientas
							for (var i = 0; i < $scope.herramientas.length; i++) {
								for (var j = 0; j < $scope.ConocimientosUser.length; j++) {
									var equal = angular.equals(
											$scope.herramientas[i].herramienta,
											$scope.ConocimientosUser[j].herramienta);

									if (equal == false) {
										n++;
									}
								}
								if(n==con){
									if($scope.herramientas[i].herramienta!=null){
										$scope.Listaconocimientos.push($scope.herramientas[i].herramienta);
									}
								}
								n=0;
							}
							

							if($scope.Listaconocimientos.length==0){
								$scope.opcionesNuevo=true;
								$scope.Noherramientas="No existen herramientas disponibles";
								//alert("No hay más conocimientos disponibles");
							}
						}
						else{
							//validación para los usuarios que no tienen conociemientos en su lista
							for (var i = 0; i < $scope.herramientas.length; i++) {
								$scope.Listaconocimientos.push($scope.herramientas[i].herramienta);
							}
							$scope.ConocimientosUser=[];
						}

						//obtención de herramientas existentes
					}, function errorCallback(response) {
						JL("ERROR").error("Error al consultar servicio: buscarHerramienta, response: "+ response.data.error);
					});

				//obtención de Conocimientos del usuario
			}, function errorCallback(response) {
				JL("ERROR").error("Error al consultar servicio: buscarRecursoXUsuario , response: "+  response.data.error);
			});

	};

	$scope.cierraModal = function(){
		for(var i=0;i<$scope.ConocimientosUser.length;i++){

			if($scope.ConocimientosUser[i].edicionNivel== true){
				
				$scope.ConocimientosUser[i].edicionNivel=false;
			}
		}
		
		$scope.agregadoOK=false;
		$scope.opcionesNuevo=false;
		$scope.Noherramientas="";
		$scope.Init();
	}


	//$scope.Init();

	$scope.$on('actualizar-header', function (event, args) {
		//console.log(args);
		$scope.Init();
	})

	//AGREGAR conocimiento nuevo
	$scope.AgregaConocimiento = function () {
		if($scope.ConocimientosUser!=null){
			for(var i=0;i<$scope.ConocimientosUser.length;i++){
				//Condición para de agregar un nuevo registro 
				if($scope.ConocimientosUser[i].edicionConoc== true || 
						$scope.ConocimientosUser[i].edicionNivel== true ){
					$scope.ConocimientosUser[i].edicionConoc=false;
					$scope.ConocimientosUser[i].edicionNivel=false;

				}
				else
					if($scope.ConocimientosUser[i].edicionNivel== true){
						//Condición de editar el nivel de algún conocimiento
						$scope.ConocimientosUser[i].edicionNivel=false;

					}
			}
		}


		//Agregar conocimiento a la lista del recurso
		$http.get($rootScope.url+ 'buscarRecursoXUsuario/'+$rootScope.currentUser.login ,
                  {headers: {'Authorization': 'Bearer ' + loginData.access_token}})
			.then(function successCallback(response) {
				if (response.data != ""){
					var Informacion = {};
					Informacion.idRecurso =response.data.idRecurso;
					//console.log("idRecurso "+ Informacion.idRecurso);
					Informacion.usuario = response.data.usuario;
					//console.log("usuario "+ Informacion.usuario);
					Informacion.nombre = response.data.nombre;
					//console.log("nombre "+ Informacion.nombre);
					Informacion.proyectos = response.data.proyectos;
					//console.log("proyectos "+ Informacion.proyectos[1]);
					Informacion.conocimientos=$scope.ConocimientosUser;
					//console.log("conocimientos "+ Informacion.conocimientos.length);

					$http.put($rootScope.url + 'actualizarRecurso', Informacion,
							{headers: {'Authorization': 'Bearer ' + loginData.access_token}})
							.then(function successCallback(Response){
								//console.log("Se agregó/actualizó  conocimiento a recurso "+Informacion.idRecurso);
								//JL("INFO").info("Se agregó/actualizó  conocimiento");
								$scope.proyectoGuardado = Response;
								$scope.success();
								$scope.Init();

							},function errorCallback(Response){
								$scope.DatosProyectoGuardadoError = Response.data;
								$scope.danger();
								JL("ERROR").error("Error al consumir servicio actualizarRecurso, response: "+  response.data.error);
								
								
							});

				}//fin del if si no trae datos
				else
				{
					console.log("El usuario no tiene información(Error en insertar/actualizar) ");
					}
			},function errorCallback(Response){
				JL("ERROR").error("Error al consumir servicio buscarRecursoXUsuario, response: "+  response.data.error);
			}); //fin del método inserta


	}

	//NUEVO Conocimiento
	$scope.InputsConocimiento = function(){
		$scope.opcionesNuevo=true;
		$scope.opcionesAgregar=false;
		//bloquear boton de nuevo para solo agregar uno a la vez
		if($scope.ConocimientosUser!=null){
			for(var i=0;i<$scope.ConocimientosUser.length;i++){
				if($scope.ConocimientosUser[i].edicionNivel== true){
					//Condición para desactivar la edicion de los niveles
					$scope.ConocimientosUser[i].edicionNivel=false;
				}
			}

			$scope.ConocimientosUser.splice(0, 0, {herramienta:$scope.Listaconocimientos[0], nivel:$scope.Niveles[0], edicionConoc:true,edicionNivel:true });
			
		}
		else
			$scope.ConocimientosUser.push({herramienta:$scope.Listaconocimientos[0], nivel:$scope.Niveles[0], edicionConoc:true,edicionNivel:true });
	}

	$scope.editarConocimiento = function (ser) {
		ser.edicionNivel = true;
		$scope.opcionesAgregar=false;
		$scope.agregadoOK=false;
		$scope.Noagregado=false;
	}          

	$scope.goProjects = function(){
		$location.path("/TechPM/Proyectos");
	};

	$scope.goClients = function(){
		$location.path("/TechPM/Clientes");
	};




}])