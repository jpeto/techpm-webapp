app.controller('agregarClientes', [ '$scope', '$location', '$rootScope', '$cookieStore', '$http', '$q', '$uibModalInstance', 'Flash', '$timeout', '$window',
	function($scope, $location, $rootScope, $cookieStore, $http, $q, $uibModalInstance,Flash,$timeout,$window) {
    loginData = $rootScope.currentUser;
	$cookieStore.put('vista', $location.path());
	
	$scope.nombre = "";
	$scope.rutaImagen = "";
	$scope.nombreProyecto = "";
	$scope.seccion = "logos";
	$scope.files = [];
	
	//Limpiar Variables 
	var limpiarPopCliente = function() {
		$scope.nombre = "";
		$scope.rutaImagen = "";
		$scope.nombreProyecto = "";
		document.getElementById("imageId").value = "";
		document.getElementById("uploadFile").value = "";
	}
	
	//Mensajes De Respuesta
	$scope.DatosClienteActualizadoError = "";
	
    $scope.success = function () {
        var message = '<strong>Excelente!</strong> El Cliente : <strong>'+ $scope.clienteGuardado.data.nombre + '</strong>' +
        	' y el proyecto <strong>'+ $scope.proyectoGuardado.data.nombreProyecto + '</strong> fue agregado con exito, Estatus: ' 
        		+ '<strong>' + $scope.clienteGuardado.status + '-' + $scope.clienteGuardado.statusText + '</strong>';
        Flash.create('success', message);
    };
    
    $scope.danger = function () {
        var message = '<strong>Lo sentimos!</strong> El proceso no se pudo completar, error : ' 
        		+ '<strong>' + $scope.DatosClienteActualizadoError.status + '-' + $scope.DatosClienteActualizadoError.message + '</strong>';
        Flash.create('danger', message);
    };
    
    $scope.warning = function () {
        var message = '<strong>Lo sentimos!</strong> El cliente : ' 
        		+ '<strong>' + $scope.nombre + '</strong> Ya existe';
        Flash.create('warning', message);
    };
	
	$scope.guardar = function() {
		$scope.getRutaSeccionCliente();
		var clienteExiste = false;
		var clienteNuevo = $scope.nombre
		$scope.submitted = true;
			$http.get($rootScope.url+ 'buscarClientes',
                      {headers: {'Authorization': 'Bearer ' + loginData.access_token}})
				.then(function successCallback(response) {
						$scope.clientesExistentes = response.data;
						for (var i = 0; i < $scope.clientesExistentes.length; i++) {
							if(angular.equals(
								$scope.clientesExistentes[i].nombre.toUpperCase(),clienteNuevo.toUpperCase())){
								clienteExiste = true;
								break;
							}
						}
						if (clienteExiste) {
							$scope.warning();
							limpiarPopCliente();
						} else {
							if ($scope.files.length > 0) {
								var cliente = {};
					            cliente.nombre = $scope.nombre;
					            cliente.imagen = $scope.rutaImagen+$scope.files[0].name;
					            cliente.credenciales = [];
					            guardarcliente(cliente);
							}else {
								var cliente = {};
					            cliente.nombre = $scope.nombre;
					            cliente.imagen = "";
					            cliente.credenciales = [];
					            guardarcliente(cliente);
							}
						}
				}, function errorCallback(response) {
					JL("ERROR").error("Error al consultar servicio: buscarClientes , response: "+  response.data.error );
				});
	}
	
	var guardarcliente = function(cliente){
		$http.put($rootScope.url+ 'guardarCliente',cliente,
				{headers: {'Authorization': 'Bearer ' + loginData.access_token}}
			).then(
				function successCallback(response) {
					$scope.clienteGuardado = response;
					var proyecto = {};
					proyecto.cliente = cliente.nombre;
					proyecto.nombreProyecto = $scope.nombreProyecto;
					proyecto.administradores = [{usuario: $rootScope.currentUser.login}];
					proyecto.branches = [];
					proyecto.herramientas = [];
					proyecto.documentos = [];
					proyecto.recursos = [];
     				proyecto.visible = true;
					proyecto.cambios=[{evidencias: []}];
					$http.put($rootScope.url+ 'guardarProyecto',proyecto,
							{headers: {'Authorization': 'Bearer ' + loginData.access_token}}
						).then(
							function successCallback(response2) {
								$scope.proyectoGuardado = response2;
								$scope.proyectoCliente = response2.data;
								$scope.idProyecto = $scope.proyectoCliente.idProyecto;
								$scope.crearCarpetas();
								$scope.submitted = false;
								$scope.uploadImageClient();
								$scope.success();
								limpiarPopCliente();
							},
							function errorCallback(response2) {
								$scope.DatosClienteActualizadoError = response2.data;
								$scope.danger();
								JL("ERROR").error(
												"Error al consultar servicio: guardarProyecto, response: "+  response.data.error);
							});
				},
				function errorCallback(response) {
					$scope.DatosClienteActualizadoError = response.data;
					$scope.danger();
					JL("ERROR").error(
									"Error al consultar servicio: guardarCliente, response: "+  response.data.error);
				});
	}
	
	$scope.cerrar = function(){
	    $uibModalInstance.dismiss('cancel');
		$rootScope.$broadcast('cerrar-modal', {cancel:'cancel'});
	}
	
	$scope.crearCarpetas = function()
	{
	    var xmlHttp = new XMLHttpRequest();
	    xmlHttp.open( "GET", "/crearCarpetas/"+$scope.idProyecto, true );
	    xmlHttp.send( null );
	    return xmlHttp.responseText;
	}
	
	//Funcion que manda a llamar el evento para escuchar input file.
	$scope.doClick = function() {
		//Funcion para llamar el input con el id asignado despues de un click
		var evento = document.querySelector("#imageId");
		if (evento) {
			evento.click();
		}
		//Funcion para optener el archivo seleccionado
		evento.addEventListener('change', function () {
	        $scope.files = this.files;
	    }, false);
		//Funcion para obtener ruta origen de archivo
	    document.getElementById("imageId").onchange = function () {
	        document.getElementById("uploadFile").value = this.value;
	    };
	}
	
    //Funcion para para mandar la ruta al server de techPMy se suba el archivo que se selecciono
	$scope.uploadImageClient = function(){
		   $scope.xhr = new XMLHttpRequest();
		   $scope.fd = new FormData();
		   $scope.xhr.open("POST", "/uploadImageClient/"+$scope.seccion, true);
		   $scope.xhr.onreadystatechange = function() {
		       if ($scope.xhr.readyState == 4 && $scope.xhr.status == 200) {
		           // Every thing ok, file uploaded
		           //console.log($scope.xhr.responseText); // handle response.
		       }
		   };
		   $scope.fd.append("userfile", $scope.files[0]);
		   $scope.xhr.send($scope.fd);
	}
	
	//Obtiene la ruta donde se almacenaran los archivos
	$scope.getRutaSeccionCliente = function(){
		var xmlHttp = new XMLHttpRequest();
	    xmlHttp.open( "GET", "/getRutaSeccionCliente/"+$scope.seccion, false );
	    xmlHttp.send( null );
	    $scope.rutaImagen = xmlHttp.responseText;
	}
}]);