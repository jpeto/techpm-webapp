app.controller('bitacoraProyecto', ['$scope', '$location', '$rootScope', '$cookieStore', '$http', 'Flash', '$window',
  function($scope, $location, $rootScope, $cookieStore, $http, Flash, $window) {

    $cookieStore.put('vista', $location.path());
      loginData = $rootScope.currentUser;

    console.log($rootScope.proyectoSeleccionado);

    $scope.proyecto = [];
    $scope.cambios = [];
    $scope.cambio = {};
    $scope.evidencias = [];
    $scope.evidenciasPorCambio = [];
    $scope.ProyectoActualizadoError = "";
    $scope.evidenciasXCambioTmp = [];
    $scope.seccion = "evidencias";
    $scope.files = [];

    $scope.initEvidencias = function() {
      for (var i = 0; i < $scope.cambios.length; i++) {
        $scope.evidencias.push($scope.cambios[i].evidencias);
      }
      console.log($scope.evidencias);
    }

    $scope.clear = function() {
      $scope.descripcion = "";
      $scope.herramienta = "";
      $scope.fechaSolicitud = "";
      $scope.fechaProductiva = "";
      $scope.qSolicito = "";
      $scope.qAutorizo = "";
      $scope.vobo = "";
      $scope.branch = "";
      $scope.estatus = "";
      $scope.commit = "";
      //arrays de evidencias son dinamicos
    }

    $scope.success = function() {
      var message = '<strong>Excelente!</strong> El cambio para el proyecto <strong>' + $scope.proyecto.nombreProyecto +
        '</strong> se ha realizado con exito';;
      Flash.create('success', message);
    };

    $scope.danger = function() {
      var message = '<strong>Lo sentimos!</strong> El proceso no se pudo completar';
      Flash.create('danger', message);
    };

    $scope.initBitacoraProyecto = function() {
      $http
        .get(
          $rootScope.url + 'buscarProyectoXId/' + $rootScope.proyectoSeleccionado, 
          {headers: {'Authorization': 'Bearer ' + loginData.access_token}})
        .then(
          function successCallback(response) {
            $scope.proyecto = response.data;
            $scope.idProyecto = $scope.proyecto.idProyecto;
            $scope.cambios = $scope.proyecto.cambios;
            $scope.proyecto.cambios[0].descripcion === null ? $scope.proyecto.cambios.splice(0, 1) : console.log('Ya fue eliminado registro vacio');
            $scope.initEvidencias();
            $scope.showTable = true;
            console.log($scope.proyecto);
            console.log($scope.proyecto.cambios);
          },
          function errorCallback(response) {
            JL("ERROR")
              .error(
                "Error al consumir servicio /buscarClienteXUsuario");
          });
    }

    $scope.initBitacoraProyecto();

    $scope.fileExists = function(fileName) {
      var xmlHttp = new XMLHttpRequest();
      xmlHttp.open("GET", "/fileExists/" + $scope.idProyecto + "/" + $scope.seccion + "/" + fileName, false);
      xmlHttp.send(null);
      return xmlHttp.responseText;
    }

    $scope.uploadFile = function(file) {
      //checa si ya existe el archivo a subir
      //existe === true ? pregunta si desea reemplazar archivo
      $scope.accionRepetido = false;
      if ($scope.fileExists(file.name) === 'true') {
        console.log('File already exists!');
        if (confirm(file.name + " ya existe\nDeseas reemplazarlo?")) {
          $scope.xhr = new XMLHttpRequest();
          $scope.fd = new FormData();
          $scope.xhr.open("POST", "/uploadFiles/" + $scope.idProyecto + "/" + $scope.seccion, false);
          $scope.xhr.onreadystatechange = function() {
            if ($scope.xhr.readyState == 4 && $scope.xhr.status == 200) {
              // Every thing ok, file uploaded
              //console.log($scope.xhr.responseText); // handle response.
            }
          };
          $scope.fd.append("userfile", file);
          $scope.xhr.send($scope.fd);
          console.log("Server response: " + $scope.xhr.responseText);
          $scope.xhr.responseText === '200' ? console.log(file.name + " uploaded successfully!") : alert("Error while uploading " + file.name);
          $scope.accionRepetido = true;
        } else {
          console.log("User cancelled the upload!")
          $scope.accionRepetido = true;
        }
      } else {
        $scope.xhr = new XMLHttpRequest();
        $scope.fd = new FormData();
        $scope.xhr.open("POST", "/uploadFiles/" + $scope.idProyecto + "/" + $scope.seccion, false);
        $scope.xhr.onreadystatechange = function() {
          if ($scope.xhr.readyState == 4 && $scope.xhr.status == 200) {
            // Every thing ok, file uploaded
            //console.log($scope.xhr.responseText); // handle response.
          }
        };
        $scope.fd.append("userfile", file);
        $scope.xhr.send($scope.fd);
        console.log("Server response: " + $scope.xhr.responseText);
        $scope.xhr.responseText === '200' ? console.log(file.name + " uploaded successfully!") : alert("Error while uploading " + file.name);
      }
    }

    var tableEvent = document.querySelector('#usser_table');
    var cells = tableEvent.getElementsByTagName("td");
    tableEvent.addEventListener('mouseover', function() {
      for (var i = 1; i < cells.length; i++) {
        var cell = cells[i];
        cell.onclick = function() {
          var cellIndex = this.cellIndex;
          $scope.rowIndex = this.parentNode.rowIndex;
          $scope.evidenciasXCambioTmp.length = 0;
          $scope.evidenciasXCambioTmp.push($scope.evidencias[$scope.rowIndex - 1]);
          $scope.safeApply(function() {});
          //console.log("cell: " + cellIndex + " / row: " + rowIndex );
          //console.log($scope.evidencias[rowIndex-1]);
        }
      }
    }, false);

    $scope.doClick = function() {
      $scope.el = document.getElementById("uploadFiles");
      if ($scope.el) {
        $scope.el.click();
      }
    }

    var getFilesFromForm = document.querySelector('#uploadFiles');
    getFilesFromForm.addEventListener('change', function() {
      $scope.files = this.files;
    }, false);

    $scope.doClickEvidencias = function() {
      $scope.el = document.getElementById("uploadEvidencias");
      if ($scope.el) {
        $scope.el.click();
      }
    }

    var getEvidenciasFromForm = document.querySelector('#uploadEvidencias');
    getEvidenciasFromForm.addEventListener('change', function() {
      $scope.agregarEvidencias(this.files);
    }, false);

    $scope.uploadFiles = function() {
      for (var i = 0; i < $scope.files.length; i++) {
        $scope.uploadFile($scope.files[i]);
      }
    }

    $scope.guardarCambios = function(flag) {
      $http.put($rootScope.url + 'actualizarProyecto', $scope.proyecto, 
                {headers: {'Authorization': 'Bearer ' + loginData.access_token}}
               ).then(function successCallBack(responseProyecto) {
          $scope.proyectoGuardado = responseProyecto;
          if (flag) {
            $scope.uploadFiles();
            $scope.clear();
          } else {
            console.log('Actualizacion');
          }
          $scope.success();
          console.log('Proyecto guardado');
          console.log($scope.proyectoGuardado);
        },
        function errorCallBack(responseCliente) {
          $scope.ProyectoActualizadoError = responseCliente.data
          $scope.danger();
          $scope.successTextAlert = 'Ocurrió un error al guardar los cambios';
          JL("ERROR")
            .error(
              "Error al consultar servicio: actualizarProyecto, estatus: " +
              $scope.ProyectoActualizadoError.status +
              ", error: " +
              $scope.ProyectoActualizadoError.error +
              ", msg: " +
              $scope.ProyectoActualizadoError.message);
        });
    }

    $scope.getRutaEvidencias = function() {
      var xmlHttp = new XMLHttpRequest();
      xmlHttp.open("GET", "/getRutaSeccion/" + $scope.idProyecto + "/" + $scope.seccion, false);
      xmlHttp.send(null);
      return xmlHttp.responseText;
    }

    $scope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if (phase == '$apply' || phase == '$digest') {
        if (fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };

    $scope.cargarEvidencias = function() {
      $scope.rutaEvidencias = $scope.getRutaEvidencias();
      $scope.evidenciasPorCambio.length = 0;
      var evid = [];
      if ($scope.files.length != 0) {
        for (var i = 0; i < $scope.files.length; i++) {
          var evidencia = {
            nombreArchivo: $scope.files[i].name,
            ruta: $scope.rutaEvidencias + $scope.files[i].name
          };
          evid.push(evidencia);
          $scope.evidenciasPorCambio.push(evidencia);
        }
        $scope.evidencias.push(evid);
      } else {
        $scope.evidenciasPorCambio = [];
        console.log("No se seleccionaron archivos");
      }
    }

    $scope.actualizaEvidencias = function(evidencia) {
      var index = $scope.proyecto.cambios[$scope.rowIndex - 1].evidencias.indexOf(evidencia);
      $scope.proyecto.cambios[$scope.rowIndex - 1].evidencias.splice(index, 1);
      $scope.evidencias.length = 0;
      $scope.evidenciasXCambioTmp.length = 0;
      for (var i = 0; i < $scope.proyecto.cambios.length; i++) {
        $scope.evidencias.push($scope.proyecto.cambios[i].evidencias);
      }
      $scope.evidenciasXCambioTmp.push($scope.evidencias[$scope.rowIndex - 1]);
      $scope.safeApply(function() {});
    }

    $scope.eliminarEvidencia = function(evidencia) {
      if ($scope.fileExists(evidencia.nombreArchivo) === 'true') {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open("GET", "/deleteFile?ruta=" + evidencia.ruta, true);
        xmlHttp.send(null);
        $scope.actualizaEvidencias(evidencia);
        $scope.guardarCambios(false);
      } else {
        alert(evidencia.nombreArchivo + " no existe.");
      }
    }

    $scope.agregarEvidencias = function(files) {
      var evids = [];
      $scope.filesEvidencias = files;
      $scope.rutaEvidencias = $scope.getRutaEvidencias();
      for (var i = 0; i < $scope.filesEvidencias.length; i++) {
        $scope.uploadFile($scope.filesEvidencias[i]);
        if ($scope.accionRepetido == false) {
          var evidencia = {
            nombreArchivo: $scope.filesEvidencias[i].name,
            ruta: $scope.rutaEvidencias + $scope.filesEvidencias[i].name
          };
          $scope.proyecto.cambios[$scope.rowIndex - 1].evidencias.push(evidencia);
          evids.push(evidencia);
        }
      }
      $scope.evidencias.push(evids);
      $scope.evidenciasXCambioTmp.push(evids);
      console.log($scope.evidenciasXCambioTmp);
      //$scope.evidenciasXCambioTmp.length === 0 ? $scope.evidenciasXCambioTmp.push(evids) : $scope.evidenciasXCambioTmp.concat(evids);
      $scope.safeApply(function() {});
      $scope.guardarCambios(false);
    }

    $scope.agregarBitacora = function() {
      $scope.cargarEvidencias();
      $scope.cambio = {
        descripcion: $scope.descripcion,
        herramienta: $scope.herramienta,
        fechaSolicitud: $scope.fechaSolicitud,
        fechaProductiva: $scope.fechaProductiva,
        qSolicito: $scope.qSolicito,
        qAutorizo: $scope.qAutorizo,
        vobo: $scope.vobo,
        branch: $scope.branch,
        estatus: $scope.estatus,
        commit: $scope.commit,
        evidencias: $scope.evidenciasPorCambio
      };
      $scope.proyecto.cambios.push($scope.cambio);
      $scope.guardarCambios(true);
    }
  }
]);
