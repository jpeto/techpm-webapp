app.controller('credencialesCliente', [ '$scope', '$location', '$rootScope', '$cookieStore', '$http', 'Flash', '$timeout', 
	function($scope, $location, $rootScope, $cookieStore,$http,Flash,$timeout) {
    
	$cookieStore.put('vista', $location.path());
	loginData = $rootScope.currentUser;
	//DATOS CREDENCIAL
	$scope.datosCliente = [];
    $scope.credenciales = [];
    $scope.credencial = {};
    
    //Datos para Actualizar 
    $scope.indice = -1;
	$scope.valueUptade = 0
	
	$scope.visible = false;
    	
    //Funcion para asignar el valor a *valueUptade* dependiendo de la accion que realizara
    //si *valueUptade == 0, selecciono nuevo
    //si *valueUptade == 1, selecciono actualizar
    $scope.clickAdd = function() {
    	$scope.valueUptade = 0;
		limpiarPopCredenciales();
	}
    
	//Funciones Tabla *Paginador, Mostrar*
	$scope.showTable = false;
	
	//Mensajes De Respuesta
	$scope.DatosCLienteActualizadoError = "";
	
    $scope.success = function () {
        var message = '<strong>Excelente!</strong> El cambio para el cliente <strong>'+ $scope.clienteGuardado.data.nombre + '</strong> fue realizado con exito, Estatus: ' 
        	+ '<strong>' + $scope.clienteGuardado.status + '-' + $scope.clienteGuardado.statusText + '</strong>';
        Flash.create('success', message);
    };
    
    $scope.danger = function () {
        var message = '<strong>Lo sentimos!</strong> El proceso no se pudo completar, error : ' 
        		+ '<strong>' + $scope.DatosCLienteActualizadoError.status + '-' + $scope.DatosCLienteActualizadoError.message + '</strong>';
        Flash.create('danger', message);
    };
    
	//Funcion para limpiar las variables
	var limpiarPopCredenciales = function() {
		$scope.ambiente = "";
		$scope.contrasena = "";
		$scope.expira = "";
		$scope.fechamovimiento = "";
		$scope.hostname = "";
		$scope.ip = "";
		$scope.protocolo = "";
		$scope.puerto = "";
		$scope.sid = "";
		$scope.usuario = "";
	    $scope.credenciales = [];
	    $scope.credencial = {};
	}
	
	var TablaQuery= function (){
		dTable = $('#user_table')
		angular.element(document).ready(function() {	    
		    dTable.DataTable({
		    	"pagingType": "full_numbers",
		    	"scrollX": true,
		    	"scrollY": 200,
		    	
		    });		   
		});
	}
	
	//Funcion para buscar las credenciales del cliente seleccionado
	$scope.initCredencialesCliente = function (){
		$http
		.get(
				$rootScope.url + 'buscarClienteXId/' + $rootScope.clienteSeleccionado.idCliente,
				{headers: {'Authorization': 'Bearer ' + loginData.access_token}})
		.then(
				function successCallback(response) {
					$scope.datosCliente = response.data;
					console.log($scope.datosCliente);
					//$scope.datosCliente.credenciales = [];
					$scope.showTable = true;
					
					var today = new Date().getTime();
					for (var i = 0; i < $scope.datosCliente.credenciales.length; ++i) {
						if($scope.datosCliente.credenciales[i].expira != null){
						var fechaFin = $scope.datosCliente.credenciales[i].expira;
						var dif = fechaFin - today;
						dias = Math.trunc(dif/(1000*60*60*24));
                        var info = dias;
						$scope.datosCliente.credenciales[i].dias = info;
						}
					}
				},
				function errorCallback(response) {
					JL("ERROR")
							.error(
									"Error al consumir servicio /buscarClienteXId , response: "+  response.data.error);
				});	
	}
	
	$scope.initCredencialesCliente();
	
	//Funcion para guardar una credencial en base de datos
	$scope.guardarCredencial = function() {	
	$http.put($rootScope.url + 'actualizarCliente', $scope.datosCliente,
					{headers: {'Authorization': 'Bearer ' + loginData.access_token}}
             ).then(function successCallBack(responseCredencial) {
						$scope.clienteGuardado = responseCredencial;
						$scope.success();
						limpiarPopCredenciales();
						$scope.initCredencialesCliente();
						$scope.dat = true;
					},
					function errorCallBack(responseCliente) {
						$scope.DatosCLienteActualizadoError = responseCliente.data
						$scope.danger();
						//$scope.successTextAlert = 'Ocurrió un error al actualizar los datos';
						JL("ERROR")
								.error(
										"Error al consultar servicio: actualizarCliente, response: "+  response.data.error);
					});
	}
	
	//Funcion para preparar la accion que se realizara y pasar los parametros que se guardaran o actualizaran
	$scope.agregarCredenciales = function() {
		$scope.fechamovimiento = new Date().toISOString().slice(0,10);
		//Condicion para saber si el usuario selecciono en el front nuevo == 0 o selecciono actualizar == 1
		if ($scope.valueUptade == 0) {
			$scope.credencial = {
				ambiente : $scope.ambiente,
				contrasena : $scope.contrasena,
				expira : $scope.expira,
				fechamovimiento : $scope.fechamovimiento,
				hostname : $scope.hostname,
				ip : $scope.ip,
				protocolo : $scope.protocolo,
				puerto : $scope.puerto,
				sid : $scope.sid,
				usuario : $scope.usuario
			};
		$scope.datosCliente.credenciales.push($scope.credencial);
		$scope.guardarCredencial();
		//Si el cliente detecta que *valueUptade* == 1 entonces el usuario selecciono actualizar registro
		}else if ($scope.valueUptade == 1) {
			$scope.credencial = {
				ambiente : $scope.ambiente,
				contrasena : $scope.contrasena,
				expira : $scope.expira,
				fechamovimiento : $scope.fechamovimiento,
				hostname : $scope.hostname,
				ip : $scope.ip,
				protocolo : $scope.protocolo,
				puerto : $scope.puerto,
				sid : $scope.sid,
				usuario : $scope.usuario
		};
		$scope.datosCliente.credenciales.splice($scope.indice,1,$scope.credencial);
		$scope.guardarCredencial();
		}
	}
	
	//Funcion para eliminar credencial seleccionada
	$scope.eliminarCredencial = function(i){
		$scope.indice =  $scope.datosCliente.credenciales.indexOf(i), 1;
		if($scope.indice > -1){
			$scope.datosCliente.credenciales.splice($scope.indice,1);
			$scope.guardarCredencial();
		}
		$scope.indice = -1;
	}
	
	//Devuelve una fecha null y setea un placeHolder
	var expiraNull = function(item) {
		if (item == null) {
			$scope.dateExpira = item;
			$scope.placeHolder = "yyyy-MM-dd";
		}else {
			$scope.dateExpira = new Date(item).toISOString().slice(0,10);
		}
	}
	
	//Funcion para cargar datos seleccionados si se va actualizar el registro 
	//si se va actualizar aqui se le asigna el valor 1 a *valueUptade*
	$scope.cargarDatosSeleccionados = function(credencial) {
		expiraNull(credencial.expira);
		$scope.valueUptade = 1
		$scope.indice = $scope.datosCliente.credenciales.indexOf(credencial), 1;
		$scope.ambiente = credencial.ambiente;
		$scope.contrasena = credencial.contrasena;
		$scope.expira = $scope.dateExpira;
		$scope.hostname = credencial.hostname;
		$scope.ip = credencial.ip;
		$scope.protocolo = credencial.protocolo;
		$scope.puerto = credencial.puerto;
		$scope.sid = credencial.sid;
		$scope.usuario = credencial.usuario;
	}
	
    //Asigna Color dependiendo a fecha de caducidad de la licencia
    $scope.setColor = function(item) {
    		if (item.dias <= 7) {
    			return {
    				borderRadius: "5px",
    				backgroundColor : "salmon"
    			}
    		}
	}
		
}]);
