app.controller('administracionRecursos', ['$scope', '$location', '$rootScope', '$cookieStore', '$http', '$window','Flash', '$timeout',
	function($scope, $location, $rootScope, $cookieStore, $http, $window, Flash,$timeout) {
    loginData = $rootScope.currentUser;
	$cookieStore.put('vista', $location.path()); 
	$scope.proyectos = [];
	$scope.descripcion="";
	$scope.detalleEstatus="";
	$scope.estatus="";
	$scope.visible= "";
	$scope.cambios= [];
	$scope.documentos=[];
	$scope.branches= [];
	$scope.herramientas = [];
	$scope.recursos = [];
	$scope.recursosProySelec=[];


	$scope.tipoMensajeAdmin="";
	$scope.MensajeAdmin="";
	$scope.proyectoSelecci = "";
	$scope.idActual="";
	$scope.nombre="";
	$scope.bloqueoUsuario="";
	$scope.ConocimientosUser=[];
	$scope.posicionProyecto=0; 
	$scope.admins=[];
	$scope.RolXusuario=[];
	$scope.Noherramientas=false;
	$scope.n="";
	$scope.models = {
			selected: null,
			list2: {"Elegidos": []},
			list1: {"Recursos": []}
	};
	$scope.administradores = {
			selected: null,
			list2: {"Elegidos": []},
			list1: {"Administradores": []}
	};
	$scope.mostrar=false;
	$scope.herrAplican=[];
	//Mensajes De Respuesta
	$scope.DatosProyectoGuardadoError = "";

	$scope.success = function () {
		var message = '<strong>Excelente!</strong> El Proyecto : <strong>'+ $scope.proyectoGuardado.data.nombreProyecto + '</strong> fue actualizado con exito, Estatus: ' 
		+ '<strong>' + $scope.proyectoGuardado.status + '-' + $scope.proyectoGuardado.statusText + '</strong>';
		Flash.create('success', message);
	};

	$scope.danger = function () {
		var message = '<strong>Lo sentimos!</strong> El proceso no se pudo completar, error : ' 
			+ '<strong>' + $scope.DatosProyectoGuardadoError.status + '-' + $scope.DatosProyectoGuardadoError.message + '</strong>';
		Flash.create('danger', message);
	};

	$scope.sucessArray = function () {
		var message = '<strong>Excelente!</strong> se agregaron administradores al proyecto: ' 
			+ '<strong>' + $scope.proyectoGuardado;
		Flash.create('success', message,4000,{container: 'flash-fixed'});
	};

	$scope.cierraModal =function(){
		$scope.tipoMensajeAdmin="";
		$scope.MensajeAdmin="";
	}

	$scope.Init = function(){


		//Obtener informacion de los proyectos del cliente actual
		$http.get($rootScope.url + 'buscarProyectoXAdmon/'+ $rootScope.currentUser.login+"/"+ $rootScope.clienteSeleccionado.nombreCliente,
                 {headers: {'Authorization': 'Bearer ' + loginData.access_token}})                 
			.then(function successCallback(response) {
				if (response.data != ""){
					$scope.proyectos = response.data;

					$scope.proyectoSeleccionado = $scope.proyectos[$scope.posicionProyecto];

					//Llenar arreglo de administradores consultado rolXusuario
					$http.get($rootScope.url+ 'consultaAdmons',
                              {headers: {'Authorization': 'Bearer ' + loginData.access_token}})
						.then(function successCallback(response) {
							if (response.data != ""){

								$scope.RolXusuario=response.data;

								$scope.changedValue($scope.proyectos[$scope.posicionProyecto]);
							}
						}, function errorCallback(response) {
							JL("ERROR").error("Error al consultar servicio: consultaAdmons/ , response: "+  response.data.error);
						});


				}
			}, function errorCallback(response) {
				JL("ERROR").error("Error al consultar servicio: buscarProyectoXAdmon/, response: "+response);
			});


	}


	$scope.Init();


	$scope.Informacion = function(item, usuario){

		$scope.nombre=item;
		$http.get($rootScope.url+ 'buscarRecursoXUsuario/'+usuario,
                  {headers: {'Authorization': 'Bearer ' + loginData.access_token}})
			.then(function successCallback(response) {
				if (response.data != ""){
					$scope.recurso = response.data;
					$scope.ConocimientosUser= $scope.recurso.conocimientos;
					$scope.nombre= $scope.recurso.nombre;
					//console.log( $scope.ConocimientosUser);
				}
			}, function errorCallback(response) {
				JL("ERROR").error("Error al consultar servicio: buscarRecursoXUsuario , response: "+  response.data.error);
			});

	}

	$scope.coincideHerrProy=function(herrProyecto,herrRecurso){
		var ok=0;
		$scope.herrAplican=[];
		if(herrProyecto==null || herrRecurso==null ){
			return ok;
		} else{

			for (var i = 0; i < herrProyecto.length ; ++i) {
				for (var j = 0; j < herrRecurso.length; ++j) {

					var equal = angular.equals(
							herrProyecto[i],
							herrRecurso[j].herramienta);

					if (equal == true) {
						ok++;
						$scope.herrAplican.push(herrProyecto[i]);
					}
				}
			}
			return ok;
		}
	}


	//Función para obtener el proyecto seleccionado
	$scope.changedValue=function(item){
		//console.log(item);
		$scope.models = {
				selected: null,
				list2: {"Elegidos": []},
				list1: {"Recursos": []}
		};
		$scope.administradores = {
				selected: null,
				list2: {"Elegidos": []},
				list1: {"Administradores": []}
		};
		$scope.Mensaje="";
		var x=0;
		//todos los administradores disponibles de rolXusuario
		for (var i = 0; i < $scope.RolXusuario.length; ++i) {
			for (var y = 0; y < item.administradores.length; ++y) {
				//console.log($scope.RolXusuario[i].usuario+" -- "+item.administradores[y].usuario);
				if($scope.RolXusuario[i].usuario==item.administradores[y].usuario){
					x++;
				}

			}

			if(x==0){
				$scope.administradores.list1.Administradores.push({usuario:$scope.RolXusuario[i].usuario});
			}
			x=0;
		}

		var buscaProy = item.nombreProyecto;
		var index = $scope.proyectos.findIndex(x=>x.nombreProyecto === buscaProy)


		$scope.posicionProyecto=index;
		$scope.idActual = item.idProyecto;
		$scope.proyectoSelecci = item.nombreProyecto;


		$scope.descripcion=item.descripcion; 
		$scope.detalleEstatus=item.detalleEstatus;
		$scope.documentos=item.documentos;

		if(item.herramientas==null){
			$scope.Noherramientas=true;

		}
		else if(item.herramientas.length==0){
			$scope.Noherramientas=true;

		}
		else{
			$scope.Noherramientas=false;
		}


		$scope.herramientas = item.herramientas;
		$scope.estatus=item.estatus;
		$scope.visible= item.visible;
		$scope.cambios= item.cambios;
		$scope.branches= item.branches;
		//administradores proyecto actual
		if(item.administradores!=null){
			for (var i = 0; i < item.administradores.length; ++i) {
				//console.log(item.administradores[i].usuario+" ++ "+$rootScope.currentUser.login);
				if(item.administradores[i].usuario!=$rootScope.currentUser.login){
					$scope.administradores.list2.Elegidos.push({usuario:item.administradores[i].usuario});
				}
			}
		}

		$scope.admins=item.administradores;
		$scope.recursosProySelec=item.recursos;
		
		
		if(item.recursos!=null){
			for (var i = 0; i < item.recursos.length; ++i) {

				$scope.models.list2.Elegidos.push(item.recursos[i]);
			}
		}

		//Obtener  recursos disponibles
		$http.get($rootScope.url + 'buscarRecurso', 
                  {headers: {'Authorization': 'Bearer ' + loginData.access_token}})                 
			.then(function successCallback(response) {
				if (response.data != ""){
					$scope.recursos = response.data;
					//console.log($scope.recursos);

					var n=0;
					if(item.recursos!=null){

						//Si contiene recursos el proyecto
						var con= item.recursos.length;
						for (var i = 0; i < $scope.recursos.length; ++i) {
							for (var j = 0; j < item.recursos.length; ++j) {
								var equal = angular.equals(
										$scope.recursos[i].usuario,
										item.recursos[j].usuario);
								if (equal == false) {
									n++;
								}
							}
							//Variable para detectar de que lista es el recurso
							$scope.recursos[i].source = "recurso";
							//Condición para no repetir recursos
							if(n==con){
								//Condicion para agregar a la lista recursos disponibles 
								var igual= $scope.coincideHerrProy(item.herramientas,$scope.recursos[i].conocimientos);
								//console.log(igual);
								if(igual>=1){
									if($scope.recursos[i].usuario!= $rootScope.currentUser.login){
										$scope.models.list2.Elegidos.push({nombre:$scope.recursos[i].nombre, 
											usuario:$scope.recursos[i].usuario, 
											conocimiento:$scope.recursos[i].conocimientos, 
											source:$scope.recursos[i].source});
									}
								}
								else{
									//condicion para agregar a la lista de posibles recursos del proyecto
									if($scope.recursos[i].usuario!= $rootScope.currentUser.login){

										$scope.models.list1.Recursos.push({nombre:$scope.recursos[i].nombre, 
											usuario:$scope.recursos[i].usuario, 
											conocimiento:$scope.recursos[i].conocimientos, 
											source:$scope.recursos[i].source});
									}
								}
							}

							n=0;
						}
					}
					//Si no contiene recursos el proyecto
					else {

						for (var i = 0; i < $scope.recursos.length; ++i) {
							//Variable para detectar de que lista es el recurso
							$scope.recursos[i].source = "recurso";
							//Condicion para agregar a la lista recursos disponibles 
							var igual= $scope.coincideHerrProy(item.herramientas,$scope.recursos[i].conocimientos);
							//console.log(igual);
							if(igual>=1){
								if($scope.recursos[i].usuario!= $rootScope.currentUser.login){
									$scope.models.list2.Elegidos.push({nombre:$scope.recursos[i].nombre, 
										usuario:$scope.recursos[i].usuario, 
										conocimiento:$scope.recursos[i].conocimientos, 
										source:$scope.recursos[i].source});
								}
							}
							else{
								//condicion para agregar a la lista de posibles recursos del proyecto
								if($scope.recursos[i].usuario!= $rootScope.currentUser.login){
									$scope.models.list1.Recursos.push({nombre:$scope.recursos[i].nombre, 
										usuario:$scope.recursos[i].usuario, 
										conocimiento:$scope.recursos[i].conocimientos, 
										source:$scope.recursos[i].source});
								}
							}
						}

					}
				}
			}, function errorCallback(response) {
				JL("ERROR").error("Error al consultar servicio: buscarRecurso , response: "+  response.data.error);
			});


	}

	$scope.setColor = function(item){
		if(item.source ==  "recurso"){
			return {"background-color": "white",
				"color" : "black"};
		}else{
			return {"background-color": "#9C7D7A"
				,
				"color" : "white"};
		}
	}

	$scope.HerramientasRecursos=function(conocimientos){
		var herramientas=[];
		//se identifica si viene de recursos, o ya pertenecia al proyecto
		if(conocimientos[0].herramienta==null){
			for (var i = 0; i < conocimientos.length; ++i) {
				herramientas.push(conocimientos[i]);

			}
		}else{
			for (var i = 0; i < conocimientos.length; ++i) {
				herramientas.push(conocimientos[i].herramienta);

			}
		}


		return herramientas
	}

	$scope.GuardaProyecto=function(){
		$scope.AgregaRecursos("Guarda");
	}

	$scope.AgregaRecursos=function(opc){
		

		var RecursosNuevos= [];
		var proyecto = {};
		for (var i = 0; i < $scope.models.list2.Elegidos.length; ++i) {
			var herr=[];

			herr= $scope.HerramientasRecursos($scope.models.list2.Elegidos[i].conocimiento);
			//console.log("Usuario: "+ $scope.models.list2.Elegidos[i].nombre +"herr "+herr);
			RecursosNuevos.push({nombre:$scope.models.list2.Elegidos[i].nombre, 
				usuario:$scope.models.list2.Elegidos[i].usuario, 
				conocimiento:herr});

		}
		if(opc=='Guarda'){
			proyecto.recursos = RecursosNuevos;
		}
		else
		if(opc=='GuardarAdmins'){
			
			proyecto.recursos = $scope.recursosProySelec;
			
		}




		
		proyecto.idProyecto= $scope.idActual;
		proyecto.cliente = $rootScope.clienteSeleccionado.nombreCliente;
		proyecto.nombreProyecto = $scope.proyectoSelecci;
		proyecto.descripcion= $scope.descripcion;
		proyecto.detalleEstatus= $scope.detalleEstatus;
		proyecto.herramientas=$scope.herramientas;
		proyecto.estatus= $scope.estatus;
		proyecto.visible= $scope.visible;

		proyecto.administradores= $scope.admins;
		proyecto.cambios=$scope.cambios;
		proyecto.documentos= $scope.documentos;
		proyecto.branches= $scope.branches;

		//console.log(proyecto);
		$http.put($rootScope.url + 'actualizarProyecto', 
				proyecto,
				{headers: {'Authorization': 'Bearer ' + loginData.access_token}})
				.then(function successCallback(Response){
					$scope.proyectoGuardado = Response;

					if(opc=='Guarda'){
						$scope.success();

					}
					if(opc=='GuardarAdmins'){
						$scope.proyectoGuardado = $scope.proyectoSelecci;
						$scope.sucessArray();
					}

					$scope.Init();

				},function errorCallback(Response){
					$scope.DatosProyectoGuardadoError = Response.data;
					$scope.danger();
					JL("ERROR").error("Error al consultar servicio: actualizarProyecto , response: "+  response.data.error);

				});

	}

	$scope.Muestra= function(item){
		$scope.coincideHerrProy($scope.herramientas,item.conocimiento);
		
		var concat="";
		for (var i = 0; i < $scope.herrAplican.length; ++i) {
			concat= concat+ $scope.herrAplican[i]+ ",";
		}
		
		var pos = concat.lastIndexOf(",");
		concat = concat.substring(0,pos)+""+concat.substring(pos+1);
		
		$scope.n=concat;
	}

	$scope.AgregaAdmin= function(){
		$scope.admins=[];
		//
		for (var i = 0; i < $scope.administradores.list2.Elegidos.length; ++i) {

			$scope.admins.push({usuario:$scope.administradores.list2.Elegidos[i].usuario});	

		}
		$scope.admins.push({usuario:$rootScope.currentUser.login});	
		$scope.AgregaRecursos("GuardarAdmins");
	}


}]);