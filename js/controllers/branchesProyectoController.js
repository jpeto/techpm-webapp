app.controller('branchesProyecto', [ '$scope', '$location', '$rootScope', '$cookieStore', '$http', '$window', 'Flash',
	function($scope, $location, $rootScope, $cookieStore, $http, $window,Flash) {
    
	$cookieStore.put('vista', $location.path()); 
	loginData = $rootScope.currentUser;
	//console.log($rootScope.proyectoSeleccionado);

	$scope.proyectoBranch = "";
	$scope.branch = {};

	$scope.nombre = "";
	$scope.descripcion = "";
	$scope.url = "";
	$scope.productiva = "";
	$scope.permisos = "";
	$scope.indice = -1;
	
	//Funciones Tabla *Paginador, Mostrar*
	$scope.showTable = false;
	
	var limpiarPopBranches = function() {
		$scope.nombre = "";
		$scope.descripcion = "";
		$scope.url = "";
		$scope.productiva = "";
		$scope.permisos = "";
	    $scope.branch = {};
	}
	
	var TablaQuery= function (){
		dTable = $('#user_table')
		angular.element(document).ready(function() {	    
		    dTable.DataTable({
		    	"pagingType": "full_numbers",
		    	"scrollX": true,
		    	"scrollY": 200,
		    	
		    });		   
		});
	}

	$scope.success = function () {
		var message = '<strong>Excelente!</strong> Branch Agregado/actualizado Correctamente';
		Flash.create('success', message);
    };
    
    $scope.danger = function () {
        var message = '<strong>Lo sentimos!</strong> El proceso no se pudo completar';
        Flash.create('danger', message);
    };
	
	$scope.initBranchesProyecto = function (){

		$http.get($rootScope.url + 'buscarProyectoXId/' + $rootScope.proyectoSeleccionado, 
				{headers: {'Authorization': 'Bearer ' + loginData.access_token}})
		.then(function successCallback(response) {
			        $scope.proyectoBranch = response.data;
					$scope.showTable = true;
					//console.log($scope.proyectoBranch);
				},
				function errorCallback(response) {
					JL("ERROR").error("Error al consultar servicio: buscarProyectoXId/ , response: "+  response.data.error);
				});	

	}
	
	$scope.initBranchesProyecto();

	
	$scope.guardarBranches = function() {	
		$http.put($rootScope.url + 'actualizarProyecto', $scope.proyectoBranch,
					{headers: {'Authorization': 'Bearer ' + loginData.access_token}}
                 ).then(function successCallBack(responseProyecto) {
							$scope.proyectoGuardado = responseProyecto.data;
							limpiarPopBranches();
							$scope.success();
						},
						function errorCallBack(responseProyecto) {
							$scope.danger();
							$scope.successTextAlert = 'Ocurrió un error al actualizar los datos';
							JL("ERROR").error("Error al consultar servicio: actualizarProyecto , response: "+  response.data.error);
						});
	}
	


	$scope.agregarBranch = function() {
		$scope.branch = {
				nombre : $scope.nombre,
				descripcion : $scope.descripcion,
				url : $scope.url,
				productiva : $scope.productiva,
				permisos : $scope.permisos
		};

		$scope.proyectoBranch.branches.push($scope.branch);
		$scope.guardarBranches();
		//console.log($scope.proyectoBranch);
	}

	$scope.eliminarBranch = function(){
		if($scope.indice > -1){
		   $scope.proyectoBranch.branches.splice($scope.indice,1);
		   $scope.guardarBranches();
		}
		$scope.indice = -1;
	}
	
	$scope.confirmar = function(i){
		$scope.indice = i;
	}


	$scope.editar = function(branch,i) {
		$scope.indice = i;
		$scope.nombreEd = branch.nombre;
		$scope.descripcionEd = branch.descripcion;
		$scope.urlEd = branch.url;
		$scope.productivaEd = branch.productiva;
		$scope.permisosEd = branch.permisos;
	}

	$scope.actualizarBranch = function(){
		$scope.branch = {
			nombre : $scope.nombreEd,
			descripcion : $scope.descripcionEd,
			url : $scope.urlEd,
			productiva : $scope.productivaEd,
			permisos : $scope.permisosEd
		};
		$scope.proyectoBranch.branches.splice($scope.indice,1,$scope.branch);
		$scope.guardarBranches();
	}

}]);