app.controller('agregarHerramienta', [ '$scope', '$location', '$rootScope', '$cookieStore', '$http','$uibModalInstance', 'Flash', '$timeout',
	function($scope, $location, $rootScope, $cookieStore, $http, $uibModalInstance,Flash,$timeout) {
    loginData = $rootScope.currentUser;
	$cookieStore.put('vista', $location.path());

	$scope.herramientas = {};
	$scope.herramientaNombre= "";
	
	//Limpiar Variables 
	var limpiarPopHerramienta = function() {
		$scope.herramientaNombre = "";
	}
	
	//Mensajes De Respuesta
	$scope.DatosHerramientaActualizadoError = "";
	
    $scope.success = function () {
        var message = '<strong>Excelente!</strong> La Herramienta fue agregada con exito, Estatus:' 
        		+ '<strong>' + $scope.herramientaGuardada.status + '-' + $scope.herramientaGuardada.statusText + '</strong>';
        Flash.create('success', message);
    };
    
    $scope.danger = function () {
        var message = '<strong>Lo sentimos!</strong> El proceso no se pudo completar, error : ' 
        		+ '<strong>' + $scope.DatosHerramientaActualizadoError.status + '-' + $scope.DatosHerramientaActualizadoError.message + '</strong>';
        Flash.create('danger', message);
    };
    
    $scope.warning = function () {
        var message = '<strong>Lo sentimos!</strong> La Herramienta : ' 
        		+ '<strong>' + $scope.herramientaNombre + '</strong> Ya existe';
        Flash.create('warning', message);
    };

	$scope.guardar = function() {			
			var n=0;
			//console.log("Herramienta + : "+  $scope.herramientaNombre);
			var ingresada=$scope.herramientaNombre;
			//validar que la herramienta no exista
			$http.get($rootScope.url+ 'buscarHerramienta',
                      {headers: {'Authorization': 'Bearer ' + loginData.access_token}})
				.then(function successCallback(response) {
					if (response.data != ""){
						$scope.herramientas = response.data;
						for (var i = 0; i < $scope.herramientas.length; i++) {
							if($scope.herramientas[i].herramienta!=null){
								
								var herramienta=$scope.herramientas[i].herramienta;
								var compara = angular.equals(
										herramienta.toUpperCase(),
										ingresada.toUpperCase());

								if(compara==true){
									n++;

								}
							}
						}	
					}
					if(n==0){
						$http
						.put($rootScope.url + 'guardarHerramienta', {herramienta: $scope.herramientaNombre},
								{headers: {'Authorization': 'Bearer ' + loginData.access_token}}
						).then(
								function successCallback(response) {
									$scope.herramientas = response.data;
									$scope.herramientaGuardada = response;
									$scope.success();
									limpiarPopHerramienta();
								},
								function errorCallback(response) {
									$scope.DatosHerramientaActualizadoError = response.data;
									$scope.danger();
									JL("ERROR").error("Error al consultar servicio: guardarHerramienta/ , response: "+  response.data.error);
								});
					}else {//fin IF no herramienta repetida
						$scope.warning();
						limpiarPopHerramienta();
						n=0;
					}
					//Error en obtención de herramientas
				}, function errorCallback(response) {
					JL("ERROR").error("Error al consultar servicio: buscarHerramienta , response: "+  response.data.error );
				});

	}//fin agregar
	$scope.cerrar = function(){
	    $uibModalInstance.dismiss('cancel');
		$rootScope.$broadcast('cerrar-modal', {cancel:'cancel'});
	}

}]);