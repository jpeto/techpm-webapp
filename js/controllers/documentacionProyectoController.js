app.controller('documentacionProyecto', [ '$scope', '$location', '$rootScope', '$cookieStore', '$http', "$window", 'Flash', '$timeout',
	function($scope, $location, $rootScope, $cookieStore,$http, $window,Flash,$timeout) {
    
	$cookieStore.put('vista', $location.path());
	loginData = $rootScope.currentUser;
	//DATOS DOC
	$scope.proyectoDoc = [];
    $scope.documentos = [];
    $scope.documento = {};
    $scope.seccion = "documentacion";
    $scope.nombreDocumento = "";
	$scope.descripcion = "";
	$scope.ruta = "";
	$scope.deleteFile = "";
	$scope.files = [];
	
    //Datos para Eliminar y/o Acutalizar 
    $scope.indice = -1;
	
	//Funciones Tabla *Paginador, Mostrar*
	$scope.showTable = false;
	
	//Mensajes De Respuesta
	$scope.DatosProyectoActualizadoError = "";
	
    $scope.success = function () {
        var message = '<strong>Excelente!</strong> El cambio para el proyecto <strong>'+ $scope.proyectoGuardado.data.nombreProyecto + '</strong> fue realizado con exito, Estatus: ' 
        	+ '<strong>' + $scope.proyectoGuardado.status + '-' + $scope.proyectoGuardado.statusText + '</strong>';
        Flash.create('success', message);
    };
    
    $scope.danger = function () {
        var message = '<strong>Lo sentimos!</strong> El proceso no se pudo completar, error : ' 
        		+ '<strong>' + $scope.DatosCLienteActualizadoError.status + '-' + $scope.DatosCLienteActualizadoError.message + '</strong>';
        Flash.create('danger', message);
    };
    
    $scope.warning = function () {
        var message = '<strong>Lo sentimos!</strong> no puede haber dos archivos con el mismo nombre';
        Flash.create('warning', message);
    };
	
	//Limpiar Variables 
	var limpiarPopDocumentos = function() {
		$scope.nombreDocumento = "";
		$scope.descripcion = "";
		document.getElementById("fileInput").value = "";
		document.getElementById("uploadFile").value = "";
		$scope.documento = {};
	}
	
	var TablaQuery= function (){
		dTable = $('#user_table')
		angular.element(document).ready(function() {	    
		    dTable.DataTable({
		    	"pagingType": "full_numbers",
		    	"scrollX": true,
		    	"scrollY": 200,
		    	
		    });		   
		});
	}

	$scope.initDocumentacionProyecto = function (){

		JL("INFO").info("dentro del init" + $rootScope.proyectoSeleccionado);

		$http.get($rootScope.url + 'buscarProyectoXId/' + $rootScope.proyectoSeleccionado, 
				{headers: {'Authorization': 'Bearer ' + loginData.access_token}})
		.then(function successCallback(response) {
			        $scope.datosProyecto = response.data;
			        console.log($scope.datosProyecto);
			        $scope.idProyecto =  $scope.datosProyecto.idProyecto;
					$scope.showTable = true;
				},
				function errorCallback(response) {
					JL("ERROR").error("Error al consumir servicio /buscarProyectoXId");
				});	

	}
	
	$scope.initDocumentacionProyecto();
	
	$scope.guardarDocumentos = function() {
		$http.put($rootScope.url + 'actualizarProyecto', $scope.datosProyecto,
					{headers: {'Authorization': 'Bearer ' + loginData.access_token}}
                 ).then(function successCallBack(responseProyecto) {
							$scope.proyectoGuardado = responseProyecto;
							$scope.success();
							limpiarPopDocumentos();
						},
						function errorCallBack(responseProyecto) {
							$scope.DatosProyectoActualizadoError = responseProyecto.data;
							$scope.danger();
							//$scope.successTextAlert = 'Ocurrió un error al actualizar los datos';
							JL("ERROR").error("Error al consultar servicio: actualizarProyecto, estatus: "
													+ responseProyecto.data.status
													+ ", error: "
													+ responseProyecto.data.error
													+ ", msg: "
													+ responseProyecto.data.message);
						});
	}
	
	//Funcion Para verificar si un archivo existe y regresar True
	$scope.fileExists = function(fileName){
		var xmlHttp = new XMLHttpRequest();
		xmlHttp.open( "GET", "/fileExists/"+$scope.idProyecto+"/"+$scope.seccion+"/"+fileName, false );
		xmlHttp.send( null );
		return xmlHttp.responseText;
	}
	
	//Funcion para guardar el arreglo de documentos con los nuevos parametros
	$scope.obtenerDocumentos = function() {
		if ($scope.files.length > 0) {
			$scope.documento = {
					nombreDocumento : $scope.files[0].name,
					descripcion : $scope.descripcion,
					ruta : $scope.ruta + $scope.files[0].name
			};
			$scope.datosProyecto.documentos.push($scope.documento);
			$scope.guardarDocumentos();
			$scope.uploadFile();
		}
	}
	
	//Funcion que se encarga de obtenerDocumentos y archivos dependiendo a validaciones
	$scope.agregarDocumentos = function() {
		$scope.getRutaSeccion();
		//Condicion que se encarga de verificar que se seleccionara un archivo ya que no puede estar vacio
		if ($scope.files.length > 0) {
			for (var i = 0; i < $scope.datosProyecto.documentos.length; i++) {
				//Condicion para verificar que se quiere agregar un documento que ya existe
				if ($scope.datosProyecto.documentos[i].nombreDocumento == $scope.files[0].name) {
					$scope.documentoEncontrado = $scope.datosProyecto.documentos[i];
					$scope.banderaExiste = 1; //Bandera para detectar que un archivo existe, si existe el valor sera 1
					break;
				}else if ($scope.datosProyecto.documentos[i].nombreDocumento != $scope.files[0].name){
					$scope.banderaExiste = 0;  //Bandera para detectar que un archivo no existe, si no existe el valor sera 0
				}
			}
			//Condicion si la bandera detecto que el archivo existe
			if ($scope.banderaExiste == 1) {
				var mensaje = confirm("El archivo ya existe,¿deseas remplazarlo?");
				//Condicion para saber si aceptan remplazar en archivo mensaje == true
				if (mensaje) {
						$scope.eliminarDocumentoXExiste($scope.documentoEncontrado);
						$scope.obtenerDocumentos();
				}else{
					$scope.warning(); // Mensaje para hacerle saber al usuario que no pueden existir dos documentos con el mismo nombre
					limpiarPopDocumentos();
				}
			}else {
				$scope.obtenerDocumentos(); //En caso que no exista el archivo agregar el documento sin problemas
			}
		}
	}
	
	//Obtiene la ruta donde se almacenaran los archivos
	$scope.getRutaSeccion = function(){
		var xmlHttp = new XMLHttpRequest();
	    xmlHttp.open( "GET", "/getRutaSeccion/"+$scope.idProyecto+"/"+$scope.seccion, false );
	    xmlHttp.send(null);
	    $scope.ruta = xmlHttp.responseText;
	    console.log($scope.ruta);
	}
	
	//Obtiene la ruta del archivo que se va a eliminar
	$scope.getRutaSeccionDelete = function(){
		var xmlHttp = new XMLHttpRequest();
	    xmlHttp.open( "GET", "/deleteFileSeccion/"+$scope.idProyecto+"/"+$scope.seccion+"/"+$scope.deleteFile, true );
	    xmlHttp.send(null);
	}
	
	//Funcion para descargar un archivo seleccionado en la tabla creando un elemento a
	$scope.getRutaSeccionDownload = function(registroSeleccionado){
	    var a = document.createElement("a");
	    document.body.appendChild(a);
	    a.href = "/download?ruta="+registroSeleccionado.ruta;
	    a.download = "";
	    a.click();			
	}

	//Funcion para obtener el archivo seleccionado
	var getFilesFromForm = document.querySelector('#fileInput');
    getFilesFromForm.addEventListener('change', function () {
        $scope.files = this.files;
    }, false);
    
    //Funcion para obtener ruta origen de archivo
    document.getElementById("fileInput").onchange = function () {
        document.getElementById("uploadFile").value = this.value;
    };
	
    //Funcion para para mandar la ruta al server de techPM
	$scope.uploadFile = function(){
		   $scope.xhr = new XMLHttpRequest();
		   $scope.fd = new FormData();
		   $scope.xhr.open("POST", "/uploadFiles/"+$scope.idProyecto+"/"+$scope.seccion, true);
		   $scope.xhr.onreadystatechange = function() {
		       if ($scope.xhr.readyState == 4 && $scope.xhr.status == 200) {
		           // Every thing ok, file uploaded
		           //console.log($scope.xhr.responseText); // handle response.
		       }
		   };
		   $scope.fd.append("userfile", $scope.files[0]);
		   $scope.xhr.send($scope.fd);
	}
	
	//Funcion para eliminar Documento seleccionado
	$scope.eliminarDocumento = function(registroSeleccionado){
		$scope.deleteFile = registroSeleccionado.nombreDocumento;
		$scope.deleteRuta = registroSeleccionado.ruta;
		$scope.indice =  $scope.datosProyecto.documentos.indexOf(registroSeleccionado), 1;
		if($scope.indice > -1){
			//Condicion para Verificar si el archivo existe y eliminar el documento del repositorio
			if($scope.fileExists(registroSeleccionado.nombreDocumento) === 'true'){
				$scope.datosProyecto.documentos.splice($scope.indice,1);
				$scope.guardarDocumentos();
				$scope.getRutaSeccionDelete();
			}else { //En caso contrario si el archivo no existe solo borrara el registro y no buscara el archivo para que no truene y se caiga node.js
				$scope.datosProyecto.documentos.splice($scope.indice,1);
				$scope.guardarDocumentos();
			}
		}
		$scope.indice = -1;
	}
	
	//Funcion para eliminar Documento Existente
	$scope.eliminarDocumentoXExiste = function(registroExiste){
		$scope.deleteFile = registroExiste.nombreDocumento;
		$scope.indice =  $scope.datosProyecto.documentos.indexOf(registroExiste), 1;
		if($scope.indice > -1){
			$scope.datosProyecto.documentos.splice($scope.indice,1);
		}
		$scope.indice = -1;
	}
}]);
