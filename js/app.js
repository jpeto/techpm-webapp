var app = angular.module('app', ['ngRoute', 'ngCookies', 'ngAnimate', 'uiSwitch', 'angularUtils.directives.dirPagination','dndLists','ngFlash', 'ui.bootstrap','720kb.datepicker']);


/* Constantes de eventos para autenticacion */
app.constant('AUTH_EVENTS', {
    nolog : 'auth-nolog',
	loginSuccess : 'auth-login-success',
	logoutSuccess : 'auth-logout-success',
})
 
/* Estableciendo rutas */
app.config(['$routeProvider', function($routeProvider){
    $routeProvider    
	    //Login
	    .when('/TechPM', {
	        templateUrl: 'views/login.html',
	        controller: 'login'
	      }) 	    
	    //Menu de Clientes
	    .when('/TechPM/Clientes', {
	        templateUrl: 'views/clientes.html',
	        controller: 'clientes'
	      })    
        //Menu de Proyectos
	    .when('/TechPM/Proyectos', {
	        templateUrl: 'views/proyectos.html',
	        controller: 'proyectos'
	      })
        //
	    .when('/TechPM/AdministracionRecursos', {
	        templateUrl: 'views/administracionRecursos.html',
	        controller: 'administracionRecursos'
	      })
        //
	    .when('/TechPM/ProyectosCerrados', {
	        templateUrl: 'views/proyectosCerrados.html',
	        controller: 'proyectosCerrados'
	      })
        //
	    .when('/TechPM/CredencialesCliente', {
	        templateUrl: 'views/credencialesCliente.html',
	        controller: 'credencialesCliente'
	      })
        //
	    .when('/TechPM/DetalleProyecto', {
	        templateUrl: 'views/detalleProyecto.html',
	        controller: 'detalleProyecto'
	      })
        //
	    .when('/TechPM/BitacoraProyecto', {
	        templateUrl: 'views/bitacoraProyecto.html',
	        controller: 'bitacoraProyecto'
	      })
        //
	    .when('/TechPM/BranchesProyecto', {
	        templateUrl: 'views/branchesProyecto.html',
	        controller: 'branchesProyecto'
	      })
        //
	    .when('/TechPM/DocumentacionProyecto', {
	        templateUrl: 'views/documentacionProyecto.html',
	        controller: 'documentacionProyecto'
	      })	    
	    //OtraRuta
	    .otherwise({ redirectTo: '/TechPM' });
}])

/* Verificacion de autenticacion para ir a otras rutas */
app.run(function($rootScope, autenticacion, $location,$route) {
	$rootScope.$on('$locationChangeStart', function (event) {
        if (($location.path() != '/TechPM') && (!autenticacion.isAuthenticated())) {            
            event.preventDefault();
        }
    });
});