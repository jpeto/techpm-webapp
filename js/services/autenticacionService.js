app.factory('autenticacion', [ '$http', '$rootScope', '$window', '$cookieStore', 'Session', 'AUTH_EVENTS', 
function($http, $rootScope, $window, $cookieStore, Session, AUTH_EVENTS) {
	var authService = {};
	
	//Funcion de login
	authService.login = function(user, success, error) {
        $http.post($rootScope.securityurl + '/auth/oauth/token',
                  'grant_type=password&scope=webclient&username=' + user.login + '&password=' + user.password,
            {
                headers:
                {
                    'Authorization': 'Basic Yml6cHJvOmFkbWluMTIz',
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
        .then(function successCallback(response) {
            if (response.data != ""){
                var keysRes = response.data;
                console.log(keysRes);
                
                ////////////////////////////////////////////////////////
                //Login Normal
                $http.get($rootScope.url + 'obtenerInfoUsuario/',
                     {headers: {'Authorization': 'Bearer ' + keysRes.access_token}})
                    .then(function successCallback(response) {
                    
                    if (response.data != ""){
                        var loginData = response.data;
                        $window.sessionStorage["userInfo"] = JSON.stringify(loginData);
                        //eliminar password del cliente por seguridad    
                        delete loginData.password;
                        loginData.access_token = keysRes.access_token;
                        loginData.refresh_token = keysRes.refresh_token;
                        console.log(loginData);
                        JL("INFO").info("Inicio sesion el usuario "+loginData.login); 
                        //establecer datos de sesion    
                        Session.create(loginData);
                        $rootScope.currentUser = loginData;  
                        //Disparar evento de login exitoso
                        $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                        //funcion de exito de login
                        success(loginData);
                    } else {
                        error(1);
                    }
                    
                }, function errorCallback(response) {
                        error(2);
                        console.error("Error al consultar servicio: autenticarUsuario, estatus: "+response.data.status+
                                         ", error: "+response.data.error+", msg: "+response.data.message);
                });
                ////////////////////////////////////////////////////////
            } else {
                error(1);
            }
        }, function errorCallback(response) {
                error(2);
                console.error("Error al consultar servicio: oauth, estatus: "+response.data.status+
                                 ", error: "+response.data.error+", msg: "+response.data.message);
        });
    };

    //Funcion para ver si el usuario esta autenticado
    authService.isAuthenticated = function() {
		return (Session.user != null);
	};
    
	//Funcion de logout
	authService.logout = function(){
		Session.destroy();
		$window.sessionStorage.removeItem("userInfo");
		if ($window.sessionStorage["clienteSeleccionado"]) {
			$window.sessionStorage.removeItem("clienteSeleccionado");
		}
		if ($window.sessionStorage["proyectoSeleccionado"]) {
			$window.sessionStorage.removeItem("proyectoSeleccionado");
		}
        $rootScope.currentUser = null;
        $rootScope.aplicacionSeleccionada = null;
        //Disparar evento de logout exitoso
		$rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
	}
    
    authService.refresh = function(loginData, path, success) {
        $rootScope.url = 'http://172.16.0.68:9021/techpm-service/';
        $rootScope.securityurl = 'http://172.16.0.68:9021/security/';
        $window.sessionStorage["userInfo"] = JSON.stringify(loginData);
        //establecer datos de sesion    
        Session.create(loginData);
        $rootScope.currentUser = loginData;  
        //Disparar evento de login exitoso
        $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
        //TODO: validar paths para obtener información guardada
        if (path == ''){            
        }
        //resetear demas variables guardadas
        if ($window.sessionStorage["clienteSeleccionado"]) {
        	$window.sessionStorage["clienteSeleccionado"] = JSON.stringify(JSON.parse($window.sessionStorage["clienteSeleccionado"]));
        	$rootScope.clienteSeleccionado = JSON.parse($window.sessionStorage["clienteSeleccionado"])
        }
        if ($window.sessionStorage["proyectoSeleccionado"]) {
        	$window.sessionStorage["proyectoSeleccionado"] = JSON.stringify(JSON.parse($window.sessionStorage["proyectoSeleccionado"]));
        	$rootScope.proyectoSeleccionado = JSON.parse($window.sessionStorage["proyectoSeleccionado"]);
        }
        //run success function
        success(loginData);
    }

	return authService;
}]);

app.service('Session', function() {
	this.create = function(user) {
		this.user = user;
		this.userRole = user.role;
    };
	this.destroy = function() {
		this.user = null;
		this.userRole = null;
	};
	return this;
});